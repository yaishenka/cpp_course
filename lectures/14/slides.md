---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 14.

---

# 12. Перегрузка new и delete. Аллокаторы.

6. allocator_traits
7. Использование аллокаторов в контейнерах
8. Rebinding allocators
9. Копирование и конструирование аллокаторов друг от друга

---

## 12.6 allocator_traits

Большинство методов у всех аллокаторов будут одинаковые (например construct и destroy) а еще внутри есть куча using, поэтому в c++ была добавлена специальная обертка allocator_traits
allocator_traits это структура, которая шаблонным параметром принимает класс вашего аллокатора

Почти все методы и внутренние юзинги работают по прицнипу "взять у аллокатора если есть, если нет сгенерировать автоматически"

[Статья на cppref](https://en.cppreference.com/w/cpp/memory/allocator_traits)

---

## 12.7. Использование аллокаторов в контейнерах

Сначала нужно сохранить аллокатор в полях

[Код](https://gist.github.com/yaishenka/7c175d105a3fb27a50f21eb965fb7d88) (не влезает в презентацию к сожалению)

---
Использование на примере метода reserve

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }
  // T* new_arr = new T[n]; // На неуд
  // T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]); На удос
  // T* new_arr = alloc_.allocate(n); На хор
  T* new_arr = AllocTraits::allocate(alloc_, n);  // На отл

  size_t i = 0;
  try {
    for (;i < size_; ++i) {
      AllocTraits::construct(alloc_, new_arr + i, arr_[i]); // здесь std::move(arr_[i])
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      AllocTraits::destroy(alloc_, new_arr + j);
    }
    AllocTraits::dallocate(alloc_, n);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    AllocTraits::destroy(alloc_, arr_ + i);
  }

  AllocTraits:deallocate(alloc_, arr_, capacity_)

  arr_ = new_arr;
  capacity_ = n;
}
```

---

## 12.8. Rebinding allocators

Проблема: есть класс list. Он внутри аллоцирует не T а ```Node<T>```, а аллокатор у него ```Allocator<T>```

---

Раньше было так:

```c++
class Allocator {
  template <typename U>
  struct rebind {using other = Allocator<U>; }
}
```

Тогда внутри своего класса (в данном случае List) обращаемся к ```Alloc::rebind<Node<T>::other```

Но

1. Это неудобно
2. Почти всегда одинаково у всех аллокаторов

Поэтому rebind унесли в треитс

---

```c++
template <typename T, typename Alloc = std::allocator<T>>
class List {
 public:
 private:
  using AllocTraits = std::allocator_traits<Alloc>;

  using NodeAlloc = typename AllocTraits::template rebind_alloc<Node>;
  using NodeAllocTraits = typename AllocTraits::template rebind_traits<Node>; // same as std::allocator_trais<NodeAlloc>

  NodeAlloc alloc_;
};
```

Теперь NodeAllocTraits можно пользоваться как обычным треитсом.

Проблема следующая: в полях мы храним аллокатор типа NodeAlloc, а передают нам аллокатор для типа T.
На самом деле проблема легко решается: всякий нормальный аллокатор должен уметь конструироваться от себя же с другим параметром (типом). То есть в примере выше мы просто конструируем наш NodeAlloc от объекта типа Alloc.

---

## 12.9 Копирование и конструирование аллокаторов друг от друга

Возьмем для примера PoolAllocator

```c++
PoolAlloc alloc1;
PoolAlloc alloc2 = alloc1;
```

Не очень понятно что под этим подразумевается:

- Что alloc2 просто должен работать как PoolAlloc и скопировал в себя настройки (размер пула и тд) из alloc1
- Или чтобы эти два аллокатора отвечали за один и тот же пул?

Под равенством  аллокаторов в плюсах обычно понимают следующее: один аллокатор может деаллоцировать объекты, которые аллоцировал второй аллокатор. Поэтому лучше делать так, как подразумевает ==. (Но вам никто не мешает делать по-другому)

---

Возникает проблема посерьезней:

```c++
std::vector<int, PoolAlloc> v1;
std::vector<int, PoolAlloc> v2 = v1;
```

Что в данном случае делать?

- Скопировать все элементы и сделать новйы PoolAllocator?
- Скопировать аллокатор так, чтобы он отвечал за тот же пул?

Для таких ситуаций в аллокаторе есть несколько очень специфичных юзингов и методов

---

## select_on_container_copy_construction

Этот метод нужно вызывать в конструкторе копирования. Туда передается аллокатор вектора, из которого мы конструируемся и он возвращает новый аллокатор. Поведение этого метода следующее:

- Если в аллокаторе определен метод select_on_container_copy_construction, то вызывается он
- Если метод не определен, то возвращается тот же аллокатор

--

## propagate_on_container_copy_assignemnt


В операторе присваивания копированием нужно обратиться к propagate_on_container_copy_assignemnt. Если этот юзинг выставлен в true то нам дополнительно к обычным действиям нужно забрать аллокатор (alloc_ = other.alloc_). Аналогичный юзинг есть для перемещения

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
