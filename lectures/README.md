# Лекции

## Первый семестр:

- Лекция 1 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/01) [сурс](/lectures/01/slides.md)
- Лекция 2 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/02) [сурс](/lectures/02/slides.md)
- Лекция 3 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/03) [сурс](/lectures/03/slides.md)
- Лекция 4 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/04) [сурс](/lectures/04/slides.md)
- Лекция 5 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/05) [сурс](/lectures/05/slides.md)
- Лекция 6 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/06) [сурс](/lectures/06/slides.md)
- Лекция 7 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/07) [сурс](/lectures/07/slides.md)
- Лекция 8 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/08) [сурс](/lectures/08/slides.md)
- Лекция 9 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/09) [сурс](/lectures/09/slides.md)
- Лекция 10 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/10) [сурс](/lectures/10/slides.md)
- Лекция 11 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/11) [сурс](/lectures/11/slides.md)
- Бонус: лекция про память [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/bonus) [сурс](/lectures/bonus/slides.md)

## Второй семестр:

- Лекция 12 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/12) [сурс](/lectures/12/slides.md)
- Лекция 13 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/13) [сурс](/lectures/13/slides.md)
- Лекция 14 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/14) [сурс](/lectures/14/slides.md)
- Лекция 15 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/15) [сурс](/lectures/15/slides.md)
- Лекция 16 [презентация](https://yaishenka.gitlab.io/cpp_course/lectures/15) [сурс](/lectures/16/slides.md)
