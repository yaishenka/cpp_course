---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 5.

---

# 5. Шаблоны

1. Основная идея и синтаксис
2. Работа шаблонов с точки зрения компилятора
3. Перегрузка шаблонных функций
4. Специализация шаблонов
5. Шаблонные аргументы по умолчанию
6. Non-type template parameters
7. Проблема зависимых имен
<!-- 9. Basic type-traits -->
<!-- 9. Алиасы
10. Variadic templates (since c++11)
11. Правила вывода шаблонных типов -->

---
<!-- header: "5. Шаблоны"-->

## 5.1. Основная идея и синтаксис

```c++
int GetMax(const int& a, const int& b) {
    return a > b ? a : b;
}

double GetMax(const double& a, const double& b) {
    return a > b ? a : b;
}

std::string GetMax(const std::string& a, const std::string& b) {
    return a > b ? a : b;
}
```

Как-то слишком много повторяющегося кода!

---

<!-- header: "5.1. Основная идея и синтаксисa"-->

Напишем по-умному:

```c++
template <typename T>
T GetMax(const T& a, const T& b) {
    return a > b ? a : b;
}
```

Пользоваться можно так:

```c++
int test = GetMax<int>(1, 2);
// или
int test = GetMax(1, 2);
```

В данном случае плюсы достаточно умны чтобы сами определить тип шаблонного аргумента

---

Итак:

* С помощью строки ```template <typename T>``` объявили шаблонный тип T и потом пользовались им в функции как обычным типом.
* Эта строка будет действовать на определение/объявление функции (и класса) которое пишется дальше. То есть область видимости, в которой видно тип T, закончится после определения/объявления функции/класса.
* T называется шаблонным параметром
* Вместо T может быть что угодно (с большой буквы CamelCase по нашему codestyle)
* Теперь у нас есть функция GetMax для всех типов (для которых определен оператор >)

---

### А что если > нет?

```c++
struct S{};
GetMax(S(), S());
```

* Будет CE

---

### А если вызвать от разных типов?

```c++
GetMax(1, 2.0);
```

* Тоже CE, потому что компилятор не знает приводить ему int к double или наоборот.

---

### А если явно указать тип?

```c++
GetMax<double>(1, 2.0);
```

* Все будет работать (могли указать int)

---

### Можно проще

```c++
template <typename T, typename U>
T GetMax(const T& a, const U& b) {
    return a > b ? a : b;
}
```

---

### Вопрос с подвохом

```c++
template <typename T, typename U>
const T& GetMax(const T& a, const U& b) {
    return a > b ? a : b;
}

GetMax(1, 2.0);
```

Какая проблема?

* вернем временно скастованный double к int по ссылке

---

### Классы тоже могуть быть шаблонными

```c++
template <typename T>
class vector {
  private:
    T* arr_ = nullptr;
};
```

---

### Псевдонимы тоже

```c++
template <typename T>
using MyMap = std::map<T, T>;

MyMap<int> map; // синоним для std::map<int, int> map;
```

---

### И константы

```c++
template <typename T>
const T pi = 3.14;
```

---

### И методы

```c++
struct Vector {
    template <typename U>
    void push_back(const U&);
}
```

---

### И методы шаблонного класса

```c++
template <typename T>
struct Vector {
    template <typename U>
    void push_back(const U&);
}
```
Чтобы определить такой метод нужно написать следующее:
```c++
template <typename T>
template <typename U>
void Vector<T>::push_back(const U&) {
    ...
}
```
***Note*** template <typename T, U> void Vector::push_back(const U&) не сработало бы!

---

### Всегда нужно использовать T и U??

Нет! Просто это общепринятые названия, но можно и так (но не нужно)

```c++
template <typename CoolTemplate>
struct Vector {
    template <typename AnotherCoolTemplate>
    void push_back(const AnotherCoolTemplate&);

    CoolTemplate value;
}

template <typename NotCoolTemplate>
template <typename AnotherNotCoolTemplate>
void Vector<NotCoolTemplate>::push_back(const AnotherNotCoolTemplate&) {
    ...
}
```

---

### typename или class??

Можно писать так:

```c++
template <class T>
T GetMax(T a, T b);
```

и ничего не поменяется :) typename и class в контексте шаблонов синонимы!

---

<!-- header: "5. Шаблоны"-->

## 5.2. Работа шаблонов с точки зрения компилятора

На самом деле на этапе препроцессинга (первый этап компиляции) генерируется код для всех шаблонных подстановок. Например:

```c++
template <typename T>
T GetMax(const T& a, const T& b) {
    return a > b ? a : b;
}
```
* Если она вообще не вызывалась, то ее и не будет в итоговом программном коде
* Если она вызывалась от int, то сгенерируется ее версия для int и тд
* Процесс подстановки шаблонов называется "инстанцирование шаблонов"

Это наглядно видно [тут](https://godbolt.org/z/P5KGsEq6x)

---

## 5.3. Перегрузка шаблонных функций

***Note*** Сейчас говорим именно про *перегрузку* а не про *специализацию*

```c++
#include <iostream>
template <typename T>
void f(T x) { std::cout << 1; }

void f(int x) {  std::cout << 2; }

int main() { f(0);}
```

* Что выведется на экран?
* Ответ - 2
* Компилятор выбрал более частный случай

---

Усложним код

```c++
#include <iostream>

template <typename T, U>
void f(T x, U y) { std::cout << 1; }

template <typename T>
void f(T x, T y) { std::cout << 2; }

void f(int x, double y) { std::cout << 3; }

int main() {
    f(0, 0);
}
```

* Что выведется на экран?
* Ответ - 2
* В данном примере 2я версия лучше 1й потому что более частная и лучше 3й потому что не надо делать приведение типов

---

Запомним два важных правила:

* Частное лучше общего
* Если есть более частная версия, но нужно будет сделать приведение типов, то лучше более общая версия

---

Посмотрим на ссылки:

```c++
#include <iostream>

template <typename T>
void f(T x) { std::cout << 1; }

template <typename T>
void f(T& x) { std::cout << 2; }

int main() {
    int x = 0;
    int& y = x;
    f(y);
}
```

* Что выведется?
* Будет CE
* Так происходит, потому что вызвать функцию от x и от y в данном контексте это одно и тоже. А для x уже будет ambiguous call

---

Добавим константную ссылку

```c++
#include <iostream>

template <typename T>
void f(T& x) { std::cout << 1; }

template <typename T>
void f(const T& x) { std::cout << 2; }

int main() {
    int x = 0;
    int& y = x;
    f(y);
}
```

* Что выведется?
* Ответ - 1
* Потому что константность еще нужно навесить

---

## 5.4 Специализация шаблонов

### 1. Сначала про классы

Допустим у нас есть вектор

```c++
template <typename T>
class Vector {
    ...
}
```

Но мы знаем что вектор для булов можно реализовать хитрей: чтобы в одном байте хранилось 8 значений. Что делать?

* Специализация!

---

```c++
template <>
class Vector<bool> {
    ...
}
```

Получили совершенно новый класс. Это называется полная специализация шаблона.

С основным шаблоном новый класс никак не связан: даже методы могут отличаться. Почему?

* Потому что при инстанцировании шаблов класс кодогенерируется заново

---

Существует частичная специализация:

```c++
template <typename T>
class Vector<T*> {
    ...
}
```

Это специализация будет работать для всех T, которые являются указателями

---

Можно специализировать не все шаблонные аргументы:

```c++
#include <iostream>

template <typename T, typename U>
class C {
    ...
}

template <typename T>
class C<T, int> {
    ...
}
```

Выбор специализации работает по тем же условным правилам, что и выбор перегрузки

---

### 2. Теперь про функции

Так как у функций существует механизм перегрузки, то понятие частичной специализации к ним не применимо. Посмотрим на следующий код:

```c++
#include <iostream>
template <typename T, typename U>
void f(T, U) { std::cout << 1; }

template <typename T>
void f(T, T) { std::cout << 2; }

template <>
void f(int, int) { std::cout << 3; }

int main() { f(0, 0); }
```

* Что выведется в поток?
* Ответ - 3
* Среди перегрузок мы выбрали вторую, так как она более частная. А у нее уже выбрали специализацию

---

А теперь передвинем:

```c++
#include <iostream>
template <typename T, typename U>
void f(T, U) { std::cout << 1; }

template <>
void f(int, int) { std::cout << 3; }

template <typename T>
void f(T, T) { std::cout << 2; }

int main() { f(0, 0); }
```

* Что выведет?
* Ответ -- 2
* Специализация применилась к 1 функции
* На моменте выбора перегрузки выиграла 2, так как она более частная
* У 2 версии нет специализации, поэтому она вызвалась сама
---

А теперь вот так:

```c++
#include <iostream>
template <typename T, typename U>
void f(T, U) { std::cout << 1; }

// тут убрали
void f(int, int) { std::cout << 3; }

template <typename T>
void f(T, T) { std::cout << 2; }

int main() { f(0, 0); }
```

* Что выведет?
* Ответ - 3
* Потому что теперь это тоже перегрузка

---

## 5.5 Шаблонные аргументы по умолчанию

```c++
template <typename T, typename Cmp = std::less<T>>
void sort(...) {
    ...
}
```

---

## 5.6. Non-type template parameters

### 1. Числовые параметры

```c++
template <typename T, int N>
struct Array {
    T arr_[N];
};
```

Важно понимать, что такой код работать не будет:

```c++
Array<int, 5> arr1;
Array<int, 10> arr2;
arr2 = arr1;
```

***Note*** Шаблонными параметры, не являющимися типами могут быть только целочисленные типы а также bool и char (неверно после C++20)


Зачем это вообще нужно? Чтобы типы от разных констант не были совместимы друг с другом

---

Время вопросов

```c++
int d = 0;
std::cin >> d;
std::array<int, y> arr1; // 1 пример

int c = 10;
std::array<int, c> arr1; // 2 пример

const int y = c;
std::array<int, y> arr1; // 3 пример

const int x = 10;
std::array<int, x> arr1; // 4 пример
```

* Какие примеры заработают?
* Ответ: только 4й

---

### 2. Шаблонные параметры шаблонов

![wtf](https://gitlab.com/yaishenka/cpp_course/-/raw/2022_2023/lectures/memes/wtf.jpg)

Да, такое есть) Давайте разбираться

---

Допустим мы пишем свой Stack и хотим передавать в шаблонах какой контейнер использовать, то есть хотим пользоваться им примерно так

```c++
template <...>
class Stack {
  public:
    ...
  private:
    Container<T> container_;
}

int main() {
    Stack<int, std::vector> s;
}
```

Чтобы это работало нужно написать следующее:

```c++
template <typename T, template <typename U> typename Container>
class Stack {
 ...
}
```
---

На самом деле шаблонным аргументов нашего шаблона мы не пользуемся, поэтому можно написать так:

```c++
template <typename T, template <typename> typename Container>
class Stack {
  public:
    ...
  private:
    Container<T> container_;
}
```

---

## 5.7. Проблема зависимых имен

```c++
template <typename T>
struct S { using X = T; };

template <>
struct S<int> { static int X;  };

int a = 0;
template <typename T>
void f() { S<T>::X * a; }
```

Строку S::X * a можно интерпретировать по разному:

* declaration - взяли X из S и объявили указатель который назвали a
* expression - взяли X из S и умножили на глобальную a

***Def*** Собственно это и есть "проблема зависимых имен", здесь X зависит от T.

---

### А как решать?

Есть следующее правило: если компилятор встречает зависимое имя и не может понять что это, то он по умолчанию считает что expression, поэтому если вызвать только f() то все будет ок

Если нужно чтобы компилятор парсил зависимое имя как declaration, то нужно написать typename (это вторая роль слова typename в C++):

```c++
template <typename T>
void f() {
    typename S<T>::X * a;
}
```

---

### И это все?

```c++
template <typename T>
struct S {
    template <int M, int N>
    struct A{};
}

template <>
struct S<int> {
    static const int A = 0;
}

template <typename T>
void f() { S<T>::A<1,2> a; }
```

Неоднозначность в строке ```S<T>::A<1,2> a;```:

* Ее можно понять как объявление переменной a типа S<T>::A<1,2>
* Ее можно понять как S<T>::A <(меньше) 1 , 2 >(больше) a

---

Даже если добавим typename это не поможет, потому что слово typename лишь означает что дальше будет название типа, а не шаблон. Чтобы это пофиксить нужно переписать функцию f вот так:

```c++
template <typename T>
void f() {
    typename S<T>::template A<1,2> a;
}
```

то есть у слова template тоже два значения в языке C++

Попробуйте придумать пример где слово typename не нужно, а слово template нужно

---
Спасибо за внимание!

![img](https://cataas.com/cat/gif)
