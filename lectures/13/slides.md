---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 13.

---

# 12. Перегрузка new и delete. Аллокаторы.

1. Перегрузка стандартных new и delete
2. Другие формы операторов new/delete
3. Идея аллокаторов
4. std::allocator

---

## 12.1 Перегрузка стандартных new и delete

Рассмотрим следующий код

```c++
struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
}
```

Как мы уже знаем в строке 1 должна выделиться память и вызваться конструктор S (в данном случае конструктор по умолчанию)

Перегрузить можно только выделение памяти

---

```c++
void* operator new(size_t n) {
  std::cout << "We are in new" << std::endl;
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete" << std::endl;
  free(p);
}

struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
  delete s;
}
```

Таким образом перегружается глобальные операторы new и delete

---

Еще можно перегрузить версиии для массивов:

```c++
void* operator new[](size_t n) {
  std::cout << "We are in new array";
  return malloc(n);
}

void operator delete[](void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}
```

---

Убедимся что перегрузки действительно работают

```c++
void* operator new(size_t n) {
  std::cout << "We are in new" << std::endl;
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete" << std::endl;
  free(p);
}

int main() {
  std::vector<int> v(10);
  v[0] = 10;
}
```

***Note*** Вектор использует new без [], позже поймем почему

У этой перегрузки есть проблема. Какая?

---

Настоящий оператор new кидает исключениеесли память выделить не получилось:

```c++
void* operator new(size_t n) {
  std::cout << "We are in new" << std::endl;

  void* ptr = malloc(n);

  if (ptr == nullptr) {
    throw std::bad_alloc();
  }

  return ptr;
}
```

***Note*** На самом деле стандартный оператор new в случае провала сначала пытается вызвать функцию std::new_handler, и только в случае ее провала бросает исключение

***Note*** Перегружать глобальный new - плохо. Скоро мы научимся это обходить

---

## 12.2 Другие формы операторов new/delete

1. Перегрузка оператора new для конкретного типа
2. placement-new

---


### 12.2.1 Перегрузка оператора new для конкретного типа

```c++
struct S {
  static void* operator new(size_t n) {
    std::cout << "We are in new for S" << std::endl;
    void* ptr = malloc(n);
    if (ptr == nullptr) {
      throw std::bad_alloc();
    }
    return ptr;
  }

  static void operator delete(void* p) {
    std::cout << "We are in delete for struct S" << std::endl;
    free(p);
  }
}
```

***Note*** Статик будет по умолчанию, но лучше не забывать писать
***Note*** При выборе какой оператор выбрать для типа, предпочтение будет отдано оператору, который объявлен внутри класса

---

### 12.2.2 placement-new

Посмотрим на такой пример:

```c++
struct S {
 private:
  S() = default;
 public:
  static void* operator new(size_t n) {
    void* ptr = malloc(n);
    if (ptr == nullptr) {
      throw std::bad_alloc();
    }
    return ptr;
  }
  static void operator delete(void* p) { free(p); }
}

int main() {
  S* p = reinterpret_cast<S*>(operator new(sizeof(S)));
  new(p) S(); // CE
  operator delete(p);
}
```
---

Пример не работает потому что если для типа определен кастомный оператор new/delete то placement new сгенерирован не будет

Удалим перегрузку

```c++
struct S {
 private:
  S() = default;
 public:
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

CE так как конструктор S приватный (так и задумано). Если конструктор сделать публичным все сработает

---

Перегрузим placement new и сделаем его другом

```c++
struct S {
  friend void* operator new(size_t, S*);
 private:
  S() = default;
 public:
}

void* operator new(size_t, S* p) {
  return p;
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

***Note*** перегрузка такая потому что часть с вызовом конструктора перегружать нельзя. А выделять память в placement new уже не нужно

Это опять не работает... Но почему?

---

Перегруженная функция не вызывает конструктор. Соответственно friend ни чего нам не дал. Придется сделать конструктор публичным :(

---

Кастомный оператор delete при этом компилятор не вызывает, кроме случая, когда в операторе new произойдет исключение

```c++
struct S {
  S() {
    throw 1;
  }
}

void* operator new(size_t n, double d) {
  std::cout << "custom new with d: " << d << std::endl;
  return malloc(n);
}

void operator delete(void* p, double d) {
  std::cout << "custom delete with d: " << d << std::endl;
  return free(p);
}

int main() {
  try {
    S* p = new(3.14) S();
  } catch (...) {}
}
```

---

## 12.3 Идея аллокаторов

Выделение памяти через new это достаточно низкоуровневая история (не такая как malloc конечно но все же).

New это абстракция над выделением памяти в C и работать с ней не очень удобно. Например: хотим для типа MyType чтобы std::vector выделял память одним способом, а std::set другим. С помощью перегрузок оператора new этого не добиться

Поэтому в C++ появилась более высокоуровневая абстракция: аллокаторы. Аллокатор это способ переопределить выделение памяти до момента обращения к оператору new

---

## 12.4 std::allocator

```c++
template <typename T>
struct Allocator {
  T* allocate(size_t n) {
    return ::operator new(n * sizeof(T));
  }

  void deallocate(T* ptr, size_t) {
    ::operator delete(ptr);
  }

  template <typename... Args>
  void construct(T* ptr, const Args&...) { // здесь на самом деле должно быть Args&&
    new(ptr) T(args...); // а здесь std::forward(args)
  }

  void destroy(T* ptr) noexcept {
    ptr->~T();
  }
}
```

---

Можно заметить что аллокатор как и new разделяет выделение памяти и вызов конструктора. Только в случае аллокатора вызов конструктора можно переопределить

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)