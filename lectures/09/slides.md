---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 9.

---

## 9. Лямда-фукции

1. Основная идея и синтаксис
2. Списки захвата
3. Лямбда функции как объекты
4. Захват полей класса и this
5. Захват с инициализацией. Захват по умолчанию (since c++14)


---

## 9.1. Основная идея и синтаксис

Довольно часто возникает ситуация когда нужна "временная функция". Например сортировка вектора

```c++
struct Cmp {
  bool operator()(int x, int y) const { return x % 10 < y % 10; }
};

int main() {
  std::vector<int> v{1, 12, 1241, 124, 1, 123, 5342, 123};
  std::sort(v.begin(), v.end(); Cmp());
  for (auto value : v) {
    std::cout << value << " ";
  }
}
```

Каждый раз заводить отдельную структуру/функцию не очень удобно

---

```c++
int main() {
  std::vector<int> v{1, 12, 1241, 124, 1, 123, 5342, 123};
  std::sort(v.begin(), v.end(), [](int x, int y) {return x % 10 < y % 10; });
  for (auto value : v) {
    std::cout << value << " ";
  }
}
```

Третий аргумент ```[](int x, int y) {return x % 10 < y % 10; }``` это и есть лямда функция

---

Лямбду можно сохранить в переменную

```c++
int main() {
  auto f = [](int x, int y) {return x % 10 < y % 10; };
  f(1,2);
}
```

***Note*** auto позволяет компилятору самому вывести тип. В данном случае это необходимо потому что мы не знаем тип лямбды.

---

Можно возвращать лямбды из функций

```c++
auto GetCompare() {
  return [](int x, int y) {return x % 10 < y % 10; };
}
```

***Note*** Да, auto можно указывать как возвращаемое значение у функций

---

Можно вызывать лямбда-функции без сохранения в переменную

```c++
int main() {
  [](int x) { std::cout << x; }(10);
}
```

---

Возвращаемое значение лямбды определяется по правилам auto (по сути по правилам вывода шаблонного типа)

Можно указать явно:

```c++
auto f = [](int x, int y) -> bool {return x % 10 < y % 10; };
```

---

## 9.2. Списки захвата

Часто в лямбда-функциях хочется обращаться к переменным объявленным снаружи:

```c++
int main() {
  int a = 10;

  [](int x) {
    std::cout << x * a;
  }(10);
}
```

Это CE. Лямбда не видит переменную a.

---

Для этого отведены [] в объявлении лямбда-функции

```c++
int main() {
  int a = 10;
  int b = 20;
  int c = 30;

  [a, b, c](int x) {
    std::cout << x * a;
  }(10);
}
```

***Note*** это называется "захват" переменной

---

По умолчанию захваченные переменные константные

```c++
int main() {
  int a = 10;
  [a]() { a += 1; }();
  std::cout << a;
}
```

То есть такой код CE

---

Можно воспользоваться ключевым словом mutable

```c++
int main() {
  int a = 10;
  [a]() mutable { a += 1; }();
  std::cout << a;
}
```

Это сделает все захваченные переменные изменяемыми.

---

Переменные можно захватывать по ссылкам

```c++
int main() {
  int a = 10;
  int b = 20;
  int c = 30;

  [&a, b, &c](int x) {
    std::cout << x * a;
  }(10);
}
```

При этом переменные a и c можно менять и это будет работать как обычная ссылка

---

## 9.3. Лямбда функции как объекты


```c++
int main() {
  auto f = [](int x, int y) {return x < y; }
  std::cout << typeid(f).name() << " " << sizeof(f);
}
```

* Сколько весит?
* Объект весит 1 байт (пустой класс с одним методом)

---

```c++
int main() {
  int a;
  auto f = [a](int x, int y) {return x < y; }
  std::cout << sizeof(f);
}
```

* А теперь сколько весит?
* 4 байта, потому что в полях хранится инт

---

Теперь должно быть понятно почему по умолчанию не можем менять поля:  оператор () генерируется константный, потому что так принято. Соответственно поля класса он менять не может.

---

Что еще можно делать с лямбда-функциями:

1. Копировать

```c++
int main() {
  auto f = [a](int x, int y) {return x < y; }
  auto ff = f;
}
```

2. Мувать (мы пока не знаем)

3. А вот присваивать уже нельзя (можно если пустой список захвата с c++20)

```c++
int main() {
  auto f = [a](int x, int y) {return x < y; }
  auto ff = f;
  ff = f;
}
```

4. Конструктор по умолчанию генерируется если пустой список захвата с c++20

---

## 9.4. Захват полей класса и this

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [](int x) {std::cout << a; return x; };
  }
};
```

Получается CE

---

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [a](int x) {std::cout << a; return x; };
  }
};
```

И так тоже CE (нельзя захватывать не локальные переменные)

---

Существует специальный способ захвата:

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [this](int x) {std::cout << a; return x; };
  }
};
```

* Что будет если лямбда будет жить дольше объекта?

---

```c++
struct S {
  int a = 1;

  auto foo() {
    auto f = [this](int x) {std::cout << a; return x; };
    return f;
  }
};

int main() {
  S* s = new S();
  auto f = s->foo();
  delete s;
  f(10);
}
```

* Что будет?
* this протухнет и будет обращение непонятно куда

---

## 9.5. Захват с инициализацией. Захват по умолчанию (since c++14)

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [b = a + 10](int x) {std::cout << a; return x; };
  }
};
```

То есть в правой части может быть любое выражение (работает не только в классах)

---

Еще можно инициализировать ссылку

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [&a=a](int x) {std::cout << a; return x; };
  }
}
```

***Note*** Переменные могут называться одинаково

---

Можно захватывать по константной ссылке

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [&a=std::as_const(a)](int x) {std::cout << b; return x; };
  }
};
```

---

Можно захватить все локальные переменные по значению (this в том числе)

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [=](int x) { return a; };
  }
};
```

---

Можно захватить все по ссылке

```c++
struct S {
  int a = 1;

  void foo() {
    auto f = [&](int x) { return a; };
  }
};
```

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
