---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 15.

---

# 13. Move-семантика и rvalue-ссылки

1. Описание проблемы
2. std::move: как использовать
3. Move-конструкторы, move-операторы присваивания
4. Автоматическая генерация и правило пяти
5. Исправление методов вектора
6. Определение lvalue и rvalue

---

## 13.1 Описание проблемы

Посмотрим на функцию swap:

```c++
template <typename T>
void swap(T& first, T& second) {
  T tmp = first;
  first = second;
  second = tmp;
}
```

Во всех трех строках происходит копирование, а это грустно

Если бы мы вызвали такую функцию от какого-нибудь вектора, то мы бы сделали три линейных прохода

---

Еще пример:

```c++
int main() {
  vector<std::string> v; // реализация вектора из прошлых лекций
  v.push_back(std::string("test"));
}
```

Конструктор std::string вызовется дважды (потому что push_back вызывает конструктор). Опять лишнее копирование!

---

Последнюю проблему можно исправить с помощью emplace_back:

```c++
template <typename... Args>
void emplace_back(const Args&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Он создает объект из аргументов. Это уже лучше, но мы все еще копируем аргументы и это мы поправить сейчас никак не можем

---

Еще пример: в методе resize вектора мы пользовались такой конструкцией:

```c++
std::uninitialized_copy(arr_, arr_ + size_, new_arr);
```

То есть опять копирование. А хочется просто "переложить" объекты

---

И еще пример:

```c++
std::string f() {
  return std::string("test");
}

int main() {
  std::vector<std::string> v;
  v.emplace_back(f());
}
```

Нам явно нужен новый функционал

---

## 13.2. std::move: как использовать

Перепишем swap:

```c++
template <typename T>
void swap(T& first, T& second) {
  T tmp = std::move(first);
  first = std::move(second);
  second = std::move(tmp);
}
```

И... теперь для большинства стандартных типов (movable-типов) все будет работать за О(1).

---

## Как это работает?

![](https://gitlab.com/yaishenka/cpp_course/-/raw/2022_2023/lectures/memes/magic.jpg)

---

## Как это работает?

Неформально: посмотрим на эту строчку ```T tmp = std::move(first);```

Здесь все "ресурсы" объекта first были отданы объекту tmp. Не скопированы, а просто перемещены.

Сразу важные замечания:

1. Если просто сделать std::move(object), и не присваивать ничего, то object останется таким каким и был (попозже поймем почему)
2. Все стандартные типы гарантируют, что после auto x = std::move(y), y остается валидным (пустым почти наверное).

---

## 13.3 Move-конструкторы, move-операторы присваивания

## 13.3.1 Move-конструкторы

Рассмотрим на примере класса string

```c++
class String {
 public:
  String(String&& s): str_(s.str_), size_(s.size_) {
    s.str_ = nullptr;
    s.size_ = 0;
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

---

Разберем подробней:

1. String&& - rvalue ссылка на String. С ней разберемся позже
2. ```str_(s.str_), size_(s.size_)``` - "забираем" ресурсы у донора
3. ```s.str_ = nullptr;``` - нужно занулить указатель у себя чтобы не было двойного владения (и double free)
4. ```s.size_ = 0;``` - нужно оставить объект валидным и пустым

---

## 13.3.2 Move-операторы присваивания

```c++
String operator=(String&& s) {
  String tmp = std::move(s); // Пользуемся написанным move-конструктором
  swap(tmp); // Придется написать отдельно
  reutrn *this;
}
```

---

## 13.4. Автоматическая генерация и правило пяти

Move-конструктор генерируется автоматически если нет user-declared move-конструктора и:

1. Нет user-declared copy-конструкторов
2. Нет user-declared операторов присваивания копированием
3. Нет user-declared move-операторов копирования
4. Нет user-declared деструктора

С move-оператором присваивания аналогично

Из этого следует очевидное правило пяти :) (как правило трех, только еще два пункта добавили)

---

А теперь важное: move-конструктор и move-оperator по умолчанию будут просто мувать все поля.

Давайте поймем почему такой конструктор будет плохим во многих случаях:

```c++
class String {
 public:
  String(String&& s) {
    s.str_ = std::move(s.str_);
    s.size_ = std::move(s.size_);
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

std::move от тривиальных объектов просто их копирует. То есть мы не обнулим указатель и размер у строки s, то есть сделаем double free в какой-то момент.

---

## 13.5 Исправление методов вектора

Сначала починим push_back

```c++
template <typename T>
void Vector<T>::push_back(const T& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(value);
  size_ += 1;
}

template <typename T>
void Vector<T>::push_back(T&& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::move(value));
  size_ += 1;
}
```

---

А теперь починим reserve:

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }
  T* new_arr = AllocTraits::allocate(alloc_, n);  // На отл

  size_t i = 0;
  try {
    for (;i < size_; ++i) {
      AllocTraits::construct(alloc_, new_arr + i, std::move(arr_[i]));
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      AllocTraits::destroy(alloc_, new_arr + j);
    }
    AllocTraits::dallocate(alloc_, n);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    AllocTraits::destroy(alloc_, arr_ + i);
  }

  AllocTraits:deallocate(alloc_, arr_, capacity_)

  arr_ = new_arr;
  capacity_ = n;
}
```

---

## Про emplace_back

Починить emplace_back все еще не можем: мы не знаем какие из аргументов можно мувать, а какие нет. Эту проблему мы сможем решить, когда пройдем std::forward (следующая лекция)

---

## 13.6. Определение lvalue и rvalue

Неформально мы уже говорили про эти понятия: lvalue - то что может стоять слева от =, rvalue - то что не может. На самом деле такое определение неверное, причем в обе стороны

Пример в одну сторону: объект, у которого не определен оператор присваивания не может стоять слева от =, хотя он lvalue

Пример в другую сторону: BitReference в ```vector<bool>```. Это rvalue (временно созданный объект) однако он стоял слева от =

---

***Важное*** value-category это характеристика ВЫРАЖЕНИЯ а не чего-либо еще

Итак expression может быть: glvalue, rvalue, lvalue, xvalue, prvalue

![img](https://i.stack.imgur.com/VNVwl.png)

---

Начнем с определения lvalue:

1. Идентификаторы
2. Вызов функции возвращаемый тип которой это lvalue-ссылка
3. и тд...

Общий смысл: что-то "постоянное" у чего есть имя

---

Теперь определение prvalue:

1. Литералы (кроме const char*)
2. Вызов функции возвращаемый тип которой это non-reference
3. и тд...

Общий смысл: что-то "временное" у чего нет имени

---

И определение xvalue:

1. Вызов функции тип которой это rvalue-reference
2. и тд...

Общий смысл: относится к мувнутым объектам (отсюда и название - expired)

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
