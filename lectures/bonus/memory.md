---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Калюжный Михаил. ВШПИ МФТИ 2024'
paginate: true
---

<style>
{
    font-size: 25px;
}
</style>


# Доп. Лекция

---

## Модель объектов в C++, выравнивание типов

1. Объекты в C++, требования к их хранению
2. Правильный `reserve` для `std::vector`
3. Универсальный тип для хранения неинициализированных объектов

---

## Объекты в C++

У объектов в C++ есть несколько важных свойств:

- Размер
- Требования к выравниванию (alignment requirement)
- Тип
- Значение

А также некоторые важные свойства, о которых мы задумываемся реже:

- Длительность хранения (storage duration)
  - Бывает: automatic, static, dynamic и thread_local
- Время жизни (lifetime)
  - Ограничено длительностью хранения или temporary (временный объект)

---

## Требования к хранению объектов

Для хранения кажого объекта отводится ровно `sizeof(T)` байт, при этом **не любой адрес может подходить для хранения объекта типа** `T`, так как у любого типа есть **требования к выравниванию**.

---

## Выравнивание

- **Выравнивание** - это механизм языка C++, который позволяет размещать объекты только на специальных адресах, которые удовлетворяют требованиям выравнивания объекта.

### Зачем

В отличие от нас, процессоры оперируют не байтами, а машинными словами, которые, обычно, имеют размер больше, чем 1 байт. Если сохранять объекты в памяти на произвольных адресах, то может возникнуть случай, что память выделенная под хранение объекта пересечет "естественную границу" слов в памяти. Некоторые процессоры в принципе не могут обращаться к объектам, нарушающим границы машинных слов, однако, обычно, в современных процессорах обращение к таким объектам просто медленнее, чем к выровненным.

---

## Как работает выравнивание

- Для стандартных (фундаментальных) типов выравнивание равно их размеру.
- Для пользовательских типов выравнивание (по умолчанию) равно максимальному выравниванию полей объекта. 

---

### Упражнение

Давайте найдем `sizeof(A)`, `alignof(A)`, а также отступы для всех полей `A`.

```cpp
struct A {
  int32_t a;
  int64_t b;
  int8_t c;
};
```

### Ответы

* `sizeof(A)` = 24
* `alignof(A)` = 8
* Отступ `A::a` = 0
* Отступ `A::b` = 8
* Отступ `A::c` = 16

---

## Ключевое слово `alignas`

Ключевое слово `alignas` позволяет создавать объекты с более строгими требованиями к выравниванию, чем требует их тип.

### Например

```cpp
alignas(4) int8_t a;
static_assert(alignof(int8_t) == 1);
static_assert(alignof(a) == 4);
//            ^ 'alignof(expression)' is a GNU extension
```

### Зачем

Вообще довольно бесполезно, но есть хороший пример в виде массива выровненного для кэширования.

---

## С чего все началось

В предыдущей лекции было предложенно аллоцировать внутренний буффер для вектора следующим образом:

```cpp
template <typename T>
void Vector<T>::reserve(size_t n) {
  // ...
  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  // ...
}
```

Любопытный слушатель может спросить: "А валиден ли данный код?". Для этого давайте разбираться, что об этом думает стандарт (по просьбам Артема Долты).

---

## `reinterpret_cast`

Стандарт ISO 14882:2014 (спецификация языка C++) в параграфе 5.2.10 гласит:

> An object pointer can be explicitly converted to an object pointer of a different type. When a prvalue v of object pointer type is converted to the object pointer type "pointer to *cv* T", the result is `static_cast<cv T*>(static_cast<cv void*>(v))`.

Для тех, кому лень читать, `reinterpret_cast<cv T*>(v)` - это тоже самое, что и `static_cast<cv T*>(static_cast<cv void*>(v))`.

---

## `static_cast`

1. `static_cast<cv void*>(v)`

Этот случай попадает под категорию 5.2.9/4 стандарта - существует неявное преобразование из `cv T*` в `cv void*`.

2. `static_cast<cv T*>(cv void*)`

Этот же случай попадает под категорию 5.2.9/13 стандарта - `cv void*` может быть преобразован в любой `cv T*`. Но есть одно но. В этом же параграфе говорится: 

> If the original pointer value represents the address A of a byte in memory and A satisfies the alignment requirement of T, then the resulting pointer value represents the same address as the original pointer value, that is, A. The result of any other such pointer conversion is unspecified.

---

## Что это все значит?

Это значит, что если нам не повезло, и указатель, который мы получили в результате вызова `new int8_t[n * sizeof(T)]`, не был аллоцирован на нужном адресе, то программа будем иметь **неопределенное поведение** (unspecified behaviour).

### Немного про unspecified behaviour

*Unspecified behaviour* - это брат *implementation-defined behaviour*, но хуже. Обычно, когда поведение в определенной ситуации не определено, стандарт предоставляет перечень возможных поведений в этом случае, но вендор не обязан документировать, какое из поведений он реализует в своем компиляторе. 

Проще говоря, если мы хотим писать поддерживаемый и переносимый код, мы должны избегать unspecified behaviour.

---

## Темный секрет `operator new`

Здесь может возникнуть другой вопрос:

> А как `operator new` понимает, на каких адресах он может аллоцировать объекты определенного типа и как может получиться, что полученный указатель не будет аллоцирован на нужном адресе?

Ответ прост, все вызовы к аллоцирующему `operator new` разделяются на 2 типа: *alignment-unaware* и *alignment-aware*: 

* Когда происходит вызов `new T`, где `alignof(T)` меньше или равен `__STDCPP_DEFAULT_NEW_ALIGNMENT__`, то будет задействована *alignment-unaware* перегрузка, и выравнивание указателя будет равно этой константе.
* В противном случае будут задействованы *alignment-aware* перегрузки `operator new`.

---

## Что в итоге?

Для аллокации внутреннего буффера вектора наша текущая реализация использует *alignment-unaware* `operator new`, а значит, если пользователь нашего вектора попробует сохранить в нем тип с выравниванием больше, чем `__STDCPP_DEFAULT_NEW_ALIGNMENT__`, то поведение его программы будет не определенно. 

---

## Исправляем `reserve`

Исправить наш `reserve` довольно просто. Для аллокации буффера будем использовать *alignment-aware* `operator new`.

```cpp
template <typename T>
void Vector<T>::reserve(size_t n) {
  // ...
  constexpr auto align = std::align_val_t(alignof(A));
  T* new_arr = reinterpret_cast<T*>(new(align) int8_t[n * sizeof(T)]);
  // ...
}
```

---

## Это ведь все?

К сожалению нет. 

Наш код до сих пор вызывает undefined behaviour, однако если бы я этого не сказал, вы бы и не заметили, потому что ни один компилятор не пользуется этим UB. Объяснение этого UB не войдет в эту лекцию, потому что оно заслуживает отдельной. 

Материалы для самостоятельного изучения: 

- https://en.cppreference.com/w/cpp/utility/launder
- https://www.youtube.com/watch?v=XQUMl3V_rdI

--- 

## Теперь точно все

Решение этого UB выглядит так:

```cpp
template <typename T>
void Vector<T>::reserve(size_t n) {
  // ...
  constexpr auto align = std::align_val_t(alignof(A));
  T* new_arr = reinterpret_cast<T*>(new(align) int8_t[n * sizeof(T)]);
  try {
    // ...
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      std::launder(new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }
  for (size_t i = 0; i < size_; ++i) {
    std::launder(arr_ + i)->~T();
  }
  // ...
}
```

---

## Универсальный тип для хранения неинициализированных объектов

В текущей реализации наш код не очень хорошо поддерживается. Если разработчик один раз забудет вызвать `std::launder`, то вся программа будет иметь UB, однако мы можем это исправить. На помощь нам приходит ООП. 

Давайте напишем аналог `std::optional`, который не будет хранить внутри себя информацию о валидности объекта, а будет полагаться на то, что информация о валидности искомого объекта есть у того, кто получает к нему доступ. Иначе говоря, давайте напишем класс для хранения неинициализированных переменных. 

---

### Объявление

Заголовок минимальной рабочей версии нашего класса будет выглядеть следующим образом:

```cpp
template <class T, bool Destruction>
class alignas(T) Uninitialized {
 public:
  Uninitialized() noexcept = default;

  ~Uninitialized() requires(Destruction) { (**this).~T(); }
  ~Uninitialized() noexcept requires(!Destruction) = default;

  T& operator*() noexcept;
  const T& operator*() const noexcept;

  template <class... Args>
  void Emplace(Args&&... args);

 private:
  std::array<uint8_t, sizeof(T)> bytes_;
};
```

---

### Важное замечание перед определениями

Всегда, когда вы хотите сделать параметр/аргумент шаблона типа `bool` подумайте о том, кто будет использовать ваш код. Много ли ему скажет `true` или `false` в шаблонном аргументе для нашего класса? 

Автор думает, что нет, поэтому запомните следующий паттерн:

```cpp
enum class Destruction { Needed, Skip };

template <class T, Destruction D>
class alignas(T) Uninitialized {
  // ...
};
```

Теперь пользователь нашего класса будет использовать не `Uninitialized<std::string, true>`, а `Uninitialized<std::string, Destruction::Needed>`, что автор считает более читаемым.

---

### `Emplace`

Это функция, с помощью которой, объект нашего класса записывает во внутренний буффер битовое преставление результата вычисления `T(std::forward<Args>(args)...)`. Предполагает, что во внутреннем буффере не лежит объект типа `T`.

```cpp
template <class T, Destruction D>
template <class... Args>
void Uninitialized<T, D>::Emplace(Args&&... args) {
  // don't use std::launder, because `bytes_.data` will not be dereferenced
  std::construct_at(reinterpret_cast<T*>(bytes_.data()),
                    std::forward<Args>(args)...);
}
```

---

### `operator*`

Это функция, с помощью которой, пользователь может получить доступ к объекту, представление которого лежит в буффере. Предполагает, что во внутреннем буффере лежит объект типа `T`.

```cpp
template <class T, Destruction D>
T& Uninitialized<T, D>::operator*() noexcept {
  // call std::launder to not cause UB, due to pointer conversion between incompatible types
  return *std::launder(reinterpret_cast<T*>(bytes_.data()));
}

template <class T, Destruction D>
const T& Uninitialized<T, D>::operator*() const noexcept {
  // call std::launder to not cause UB, due to pointer conversion between incompatible types
  return *std::launder(reinterpret_cast<const T*>(bytes_.data()));
}
```

---

## Переписываем вектор с помощью `Uninitialized`

Заголовок нового вектора будет выглядеть так же, за исключением типа хранящегося в буффере:

```cpp
template <typename T>
class Vector {
 public:
  // ...
 private:
  Uninitialized<T, Destruction::Skip>* arr_ = nullptr;
  // ...
};
```

---

### `reserve`

Предыдущая реализация `reserve` выглядела так:

```cpp
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }
  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  try {
    std::uninitialized_copy(arr_, arr_ + size_, new_arr);
  } catch (...) {
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }
  for (size_t i = 0; i < size_; ++i) {
    (arr_ + i)->~T();
  }
  delete[] reinterpret_cast<int8_t*>(arr_);
  arr_ = new_arr;
  capacity_ = n;
}
```

---

### `reserve`

Разберем старую реализацию по частям. Начнем с аллокации нового массива: 

```cpp
T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
```

Перепишется как:

```cpp
auto* new_arr = new Uninitialized<T, Destruction::Skip>[n];
```

Такая реализация не вызовет проблем, которые были у предыдущей, так как `Uninitialized` имеет такие же требования к выравниванию, как и `T`.

---

### `reserve`

Копирование элементов в новый буффер:

```cpp
try {
  std::uninitialized_copy(arr_, arr_ + size_, new_arr);
} catch (...) {
  delete[] reinterpret_cast<int8_t*>(new_arr);
  throw;
}
```

Перепишется как (можно сделать красивее, добавив `Uninitialized(Args&&...)`):

```cpp
try {
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i].Emplace(*(arr_[i]));
  }
} catch (...) {
  delete[] new_arr;
  throw;
}
```

---

### `reserve`

Удаление элементов из старого буффера:

```cpp
for (size_t i = 0; i < size_; ++i) {
  (arr_ + i)->~T();
}
delete[] reinterpret_cast<int8_t*>(arr_);
```

Перепишется как (будет лучше, если перегрузить `operator->`):

```cpp
for (size_t i = 0; i < size_; ++i) {
  (*arr_[i]).~T();
}
delete[] arr_;
```

---

## Что в итоге?

1. Имеем рабочую реализацию вектора, которая не вызывает UB, вне зависимости от типа элемента.
2. Бонусом имеем удобный класс для работы с неинициализированной памятью, который сильно упрощает написание кода, его восприятие, и который можно переиспользовать в будущем, например для написания `std::deque` или `std::inplace_vector`.

---

Спасибо за внимание!

![img](anya-forger.gif)
