---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 7.

---
<!-- header: ""-->

# 6. Наследование

6. Приведение типов при наследовании
7. Множественное наследование
8. Виртуальное наследование

---
<!-- header: "Наследование"-->

## 6.6. Приведение типов при наследовании

Публичное наследование позволяет кастовать ребенка к родителю:

```c++
struct Base {
  int x;
};

struct Derived: Base {
  int y;
};

int main() {
  Derived d;
  Base b = d; // 1
  Base& bb = d; // 2
}
```

* В первом случае мы сконструируем новый объект типа Base от того объекта Base который хранится внутри Derived
* Во втором случае мы просто будем ссылаться на Base который лежит внутри Derived

---

***Def*** Случай ```Base b = d``` называется "срезка при копировании"

Происходит следующая проблема: что если Base поддерживает метод, который нелогично применять к Derived.

Пример: многоугольник поддерживает метод, который сдвигает одну из точек в произвольное место. Прямоугольник такой метод поддерживать не может.

---

***Def*** Принцип подстановки Барбары Лисков: пусть q(x) является свойством, верным относительно объектов x некоторого типа T. Тогда q(y) также должно быть верным для объектов y типа S, где S является подтипом типа T.

***Note*** Принцип подстановки Барбары Лисков входит в принципы SOLID:

1. SRP (single responsibility principle) - для каждого класса должно быть определено единственное назначение. Все ресурсы, необходимые для его осуществления, должны быть инкапсулированы в этот класс и подчинены только этой задаче.
2. OCP (open-closed principle) - программные сущности должны быть открыты для расширения, но закрыты для модификации
3. LSP (Liskov substitution principle)
4. ISP (interface segregation principle) - много интерфейсов, специально предназначенных для клиентов, лучше, чем один интерфейс общего назначения
5. DIP (dependency inversion principle) - зависимость на Абстракциях. Нет зависимости на что-то конкретное

---

Простым языком: объект наследника должно быть всегда можно воспринимать как объект родителя. То есть если есть какой-то код, который ожидает получить Base а мы отдаем ему Derived то все должно работать нормально.

Мы должны стараться всегда выполнять LSP

---

## Работа кастов при private и protected наследовании

```c++
struct Base {};
struct Derived: private Base {};

int main() {
  Derived d;
  Base& b = d;
  Base bb = d;
}
```

* Сработает ли какой-то из этих кастов?
* Нет :)

---

## Обратный каст (каст вверх)

```c++
int main() {
  Base b;
  Derived& d = static_cast<Derived&>(b);
  Derived* d_ptr = static_cast<Derived*>(&b);
}
```

* Кастить "вверх" можно только указатели или ссылки (непонятно как сконструировать новый Derived из Base)
* Каст из примера будет UB, так как b никогда не является Derived на самом деле

---

Каст вверх может быть не UB:

```c++
int main() {
  Derived d;

  Base& b_ref = d;
  Base* b_ptr = &d;

  Derived& d_ref = static_cast<Derived&>(b_ref);
  Derived* d_ptr = static_cast<Derived*>(b_ptr);
}
```

С текущими нашими умениями понять UB каст или нет мы не можем. Придется подождать виртуальных функций

---

## 6.7. Множественное наследование

С++ поддерживает множественное наследование

```c++
struct Mom {};
struct Dad {};
struct Son: Mom, Dad {};
```

Размещение в пямяти работает по следующему принципу: сначала родители в том порядке, в котором они унаследованы, потом наследники. С конструкторами и деструкторами тоже самое.

---

```c++
int main() {
  Son s;
  Dad* d = &s;
}
```

d и &s могут численно не совпадать (если классы-родители не пустые) (потому что Dad лежит в Son правей начала на sizeof(Mom))

---

Проведем эксперимент:

```c++
struct Mom {int a = 1; };
struct Dad {int b = 2; };
struct Son: Mom, Dad {int c = 3; };

int main() {
  Son s;
  std::cout << *(reinterpret_cast<int*>(&s)) << std::endl;
  std::cout << *(reinterpret_cast<int*>(&s) + 1) << std::endl;
  std::cout << *(reinterpret_cast<int*>(&s) + 2) << std::endl;

  Mom* m = &s;
  std::cout << *(reinterpret_cast<int*>(m)) << std::endl;
  std::cout << *(reinterpret_cast<int*>(m) + 1) << std::endl;

  Dad* d = &s;
  std::cout << *(reinterpret_cast<int*>(m)) << std::endl;
  std::cout << *(reinterpret_cast<int*>(m) + 1) << std::endl;
}
```
---

## Проблема ромбовидного наследования (diamond inheritance problem)

```c++
struct Granny { int x; };

struct Mother: Granny { int y; };

struct Dad: Granny { int z; };

struct Son: Mother, Dad { int t;};
```

* Сколько интов будет лежать в Son?
* Ответ 5
* Как объект Son будет представлен в памяти?
* Mother(Granny, int) Dad(Granny, int) int

---

Проблема возникает в следующем коде:

```c++
int main() {
    Son s;
    s.x; // CE, ambiguous
}
```

В сыне сейчас есть два x и непонятно к какому обращаться. Все это потому то в сыне сейчас две разных бабушки:

```c++
int main() {
  Son s;
  Granny& g = s; // CE
}
```

---

При этом можно обратиться к конкретной бабушке через промежуточный каст. Ну или обратиться к конкретному полю:

```c++
int main() {
  Son s;
  s.Mom::x;
}
```

---

Еще одна проблема:

```c++
struct Granny {};
struct Mom: Granny {};
struct Son: Mom, Granny {};
```

В данном случае ко второй бабушке можно обратиться только через трюки с reinterpret_cast и сдвигами указателей

***Note*** Такая ситуация называется inaccessible base class

---

## 6.8. Виртуальное наследование

Нужно какое-то решение проблемы ромбовидного наследование. Это решение называется *виртуальное наследование*

Какие способы можно придумать чтобы по итогу хранилась одна бабушка?

---

```c++
struct Granny {int g;};
struct Mom: public virtual Granny {int m;};
struct Dad: public virtual Granny {int d;};
struct Son: Mom, Dad {int s;};
```

* Сколько будет весить Son?
* Ответ: 40

---

Теперь объект Son устроен так:

```ptr mom ptr dad son granny```

Этот ptr в первом приближении указывает на объект бабушки. Тогда вся неоднозначность пропадает.

А что будет если в иерархии есть еще виртуальное наследование от другого класса?

---

На самом деле происходит следующее: заводится специальная таблица в статической памяти в которой для каждого типа написано на какой сдвиг от начала объекта нужно переместиться чтобы найти бабушку

Этот указатель указывает на эту таблицу.

***Def*** Эта таблица называется vtable

---

<!-- header: ""-->

# 7. Динамический полиморфизм и виртуальные функции

1. Основная идея и простой пример
2. Усложненные примеры
3. Абстрактные классы и чисто виртуальные функции
4. Проблема виртуального деструктора
5. Разные уточнения
6. Приведение типов в рантайме (dynamic_cast)
7. Устройство виртуальных функций

---

## 7.1. Основная идея и простой пример

***Def*** Полиморфизм это способность функции обрабатывать данные разных типов. (За одним интерфейсом скрываются разные реализации)

Статический полиморфизм уже был:

1. Перегрузка функций
2. Шаблоны

Мы же хотим научиться выбирать функцию в рантайме. Посмотрим на стандартный пример:

---

```c++
struct Animal {
  void Say() { std::cout << "Abstract animal don't speak\n"; }
};

struct Dog: Animal {
  void Say() { std::cout << "Woof woof\n"; }
};

struct Cat: Animal {
  void Say() { std::cout << "Meow\n"; }
};

struct Bird: Animal {
  void Say() { std::cout << "Tweet\n"; }
}

void MakeAnimalTalk(Animal& animal) {
  animal.Say();
}

int main() {
  Dog d;
  MakeAnimalTalk(d); // Abstract animal don't speak
}
```

---

Исправим с помощью виртуальных функций:

```c++
struct Animal {
  virtual void Say() { std::cout << "Abstract animal don't speak\n"; }
};

struct Dog: Animal {
  void Say() { std::cout << "Woof woof\n"; }
};

struct Cat: Animal {
  void Say() { std::cout << "Meow\n"; }
};

void MakeAnimalTalk(Animal& animal) {
  animal.Say();
}

int main() {
  Dog d;
  MakeAnimalTalk(d); // Woof woof

  Cat c;
  MakeAnimalTalk(c); // Meow
}
```

---

Замечания:

* Слово virtual необходимо и достаточно писать только в родителе
* Если написать virtual еще и в наследнике то ничего не поменяется
* Если написать virtual только в наследнике, то работать будет только для его наследников
* Работает не только с ссылками но и с указателями

---

## 7.2 Усложненные примеры

```c++
struct Base {
  virtual void f() { std::cout << 1; }
};

struct Derived: Base {
  void f() const {std::cout << 2; }
};

int main() {
  Derived d;
  Base& b = d;
  b.f();
}
```

f в Derived не является виртуальной, потому что не совпадает сигнатура. То есть механизм виртуальных функций работает только при полном совпадении сигнатур.

---

Чтобы не ошибаться можно (и нужно) использовать ключевое слово override

```c++
struct Base {
  virtual void f() { std::cout << 1; }
};

struct Derived: Base {
  void f() const override {std::cout << 2; }
};

int main() {
  Derived d;
  Base& b = d;
  b.f();
}
```

Сейчас будет CE, потому что мы сказали что переопределяем какую-то виртуальную функцию родителя (использовали override), но виртуальной функции с такой сигнатурой в родителе нет.

Если убрать const (или добавить const в родительский метод) то все заработает.

---

Существует ключевое слово final, которое запрещает наследникам переопределять эту виртуальную функцию:

```c++
struct Base {
  virtual void f() { std::cout << 1; }
};

struct Derived: Base {
  void f() final {std::cout << 2; }
};

struct SubDerived: Derived {
  void f() { std::cout << 3; }
};
```

Будет CE (потому что в SubDerived переопределяется функция, которая была помечена final в родителе)

***Note*** final еще может быть использовано в наследовании: чтобы запретить дальше наследоваться

---

Посмотрим на приватность

```c++
struct Base {
  virtual void f() { std::cout << 1; }
};

struct Derived: Base {
private:
  void f() override {std::cout << 2; }
};

int main() {
  Derived d;
  Base& b = d;
  b.f();
}
```

* Что будет?
* CE не будет и выведется 2, потому что приватность проверяется на этапе компиляции, а в RE понятия приватности уже не существует. На этапе компиляции с приватностью все ок (f публичный метод Base).

---

Добавим множественное наследование

```c++
struct Mom {
    virtual void f() {std::cout << 1; }
};
struct Dad {
    void f() {std::cout << 2; }
};
struct Son: Mom, Dad {
    void f() override {std::cout << 3; }
};
int main() {
    Son s;
    Mom& m = s; m.f();
    Dad& d = s; d.f();
}
```

* Будет ли CE?
* Нет
* Что выведется?
* 32

---

```c++
struct Mom {
    virtual void f() {std::cout << 1; }
};
struct Dad {
    virtual void f() {std::cout << 2; }
};
struct Son: Mom, Dad {
    void f() override {std::cout << 3; }
};
int main() {
    Son s;
    Mom& m = s; m.f();
    Dad& d = s; d.f();
}
```

* Будет ли CE?
* Нет
* Что выведется?
* 33

---

## 7.3. Абстрактные классы и чисто виртуальные функции

Вернемся к животным. В классе Animal у нас была довольно дурацкая реализация метода Say. Более того нам бы не хотелось чтобы кто-то умел создавать объект типа Animal, потому что это всего лишь интерфейс. На помощью приходят чисто виртуальные функции:

```c++
struct Animal {
  virtual void Say() = 0;
};

struct Dog: Animal {
  void Say() { std::cout << "Woof woof\n"; }
};

struct Cat: Animal {
  void Say() { std::cout << "Meow\n"; }
};
```

Теперь Animal считается абстрактным классом

---

Для абстрактного класса верно следующее:

1. Создать объект абстрактного класса нельзя (CE)
2. Можно заводить указатели и ссылки на абстрактный класс
3. Если наследник не переопределит все чисто виртуальные методы родителя, то наследник тоже будет считаться абстрактным классом

---

Реализацию чисто виртуального метода написать все же можно:

```c++
struct Animal {
  virtual void Say() = 0;
};

void Animal::Say() {
  std::cout << "...";
}
```

От этого класс не перестанет быть абстрактным

---

## 7.4. Проблема виртуального деструктора

```c++
struct Base {};

struct Derived {
  Derived() { array = new int[10]; }

  int* array;
};

int main() {
  Base* b = new Derived();
  delete b;
}
```

* Что будет?
* Утечка памяти

---

При наследовании стоит делать деструктор виртуальным:

```c++
struct Base {
  virtual ~Base() = default;
};

struct Derived {
  Derived() {
    array = new int[10];
  }

  int* array;
};

int main() {
  Base* b = new Derived();
  delete b;
}
```

---
Спасибо за внимание!

![img](https://cataas.com/cat/gif)
