---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 16.

---

# 13. Move-семантика и rvalue-ссылки

7. rvalue-ссылки и правила их инициализации
8. Универсальные ссылки и правила сжатия ссылок
9. Реализация std::move
10. Perfect-forwarding problem и функция std::forward
11. reference qualifiers

---

## 13.7. rvalue-ссылки и правила их инициализации

Сначала вспомним про lvalue-ссылки

```c++
int main() {
  int x = 0;
  int& y = x; // y = другое имя для x
  int& h = 1; // CE
}
```

То есть lvalue-ссылку можно инициализировать только с помощью lvalue-expr.

Константную ссылку инициализировать можно даже rvalue:

```c++
const int& y = 1;
```

---

Теперь про rvalue. С rvalue-ссылками все наоборот: мы не можем инициализировать rvalue-ссылку с помощью lvalue. То есть следующий код не скомпилируется:

```c++
int main() {
  int x = 0;
  int&& y = x;
}
```

А вот такой будет работать:

```c++
int main() {
  int&& y = 0;
}
```

И даже такой:

```c++
int main() {
  int x = 1;
  int&& y = 0;
  y = x;
}
```

---

Посмотрим на следующий пример:

```c++
int main() {
  int&& x = 0;
  int&& z = x;
}
```

* Будет ли работать?
* Ответ - нет, так как x это lvalue

---

И еще

```c++
int main() {
  int&& x = 0;
  int& z = x;
}
```

* Будет ли работать?
* Ответ - да, так как x это lvalue

---

Чтобы создать rvalue-ссылку на уже существующий объект нужно воспользоваться std::move:

```c++
int main() {
  int&& x = 0;
  int&& y = std::move(x);
}
```

---

Теперь разберемся как работает перегрузка конструктора

```c++
class String {
 public:
  // 1
  String(const String& s) {
    str_ = new char[s.size_]
    memcpy(str_, s.str_, size_);
  }
  // 2
  String(String&& s): str_(s.str_), size_(s.size_) {
    s.str_ = nullptr;
    s.size_ = 0;
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

Если вызываться от lvalue то вызовется первый конструктор, потому что String&& s нельзя проинициализировать с помощью lvalue. А вот для rvalue вызовется второй конструктор.

---

## 13.8. Универсальные ссылки и правила сжатия ссылок

Хотим реализовать функцию std::move. Но есть проблема: мы не умеем писать функции, которые могут принимать и lvalue и rvalue:

```c++
foo(const type& x) // принимает и то и то, но const
foo(type& x) // принимает только lvalue
foo(type&& x) // принимает только rvalue
```

---

Поэтому в C++ придумали костыль:

```c++
template <typename T>
void f(T&& x) {
}
```
В таком контексте x будет являться универсальной ссылкой.

Цитата Мейерса: "If a variable or parameter is declared to have type T&& for some deduced type T, that variable or parameter is a universal reference."

***Note*** Вместо T может быть любое название шаблона
***Note*** Для вариадиков тоже работает

---

Чтобы понять какие у этих ссылок правила вывода, сначала разберемся с правилом сжатия ссылок (reference collapsing).

В c++ не допускается взятие ссылки на ссылку, но такое возникакает при шаблонных подстановках:

```c++
template <typename T>
void foo(T x) {
  T& y = x;
}

int main() {
  int x = 0;
  foo<int&>(x);
}
```
При подстановке получится int& & y = x, что запрещено, потому компилятор преобразует это в просто int&.

До c++11 такое поведение не было стандартизированно, а вот с появлением rvalue-ссылок правило пришлось ввести.

---

Правило очень простое: одиночный & всегда побеждает. То есть:

1. & и & это &
2. && и & это &
3. & и && это &
4. && и && это &&

---

Теперь можно понять какие правила вывода работают для универсальных ссылок:

```c++
template <typename SomeType>
void f(SomeType&& x) {}

int main() {
  f(5); // SomeType = int, decltype(x) = int&&
  int y = 5;
  f(y); // SomeType = int&!, decltype(x) = int&;
}
```

Вывод: тип будет lvalue-ссылкой или rvalue-ссылкой в зависимости от типа value которое передали в функцию

***Note*** T&& является универсальной ссылкой, только если T это шаблонный параметр этой функции. (Например в контексте push_back в векторе T&& value не является универсальной ссылкой, так как T это шаблонный параметр класса)

***Note*** При выборе перегрузки универсальные ссылки считаются предпочтительней

---

## 13.9. Реализация std::move

Заготовка понятная:

```c++
template <typename T>
? move(T&& x) noexcept {

}
```

Эта функция умеет принимать как lvalue так и rvalue. Возвращать она всегда должна rvalue

---

Подходит ли такой вариант?

```c++
template <typename T>
T&& move(T&& x) noexcept {

}
```

* Нет, потому что нам нужно всегда возвращать rvalue-ссылку, а с учетом правила сжатий ссылок такого не получится (представьте что T = int&)
* Нужно как-то убрать все ссылки с T, как это сделать?

---

Придется вспомнить про type_traits

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return ...
}
```

Что же нам написать после слова return?

* return x не подойдет (пытаемся инициализировать rvalue-ссылку с помощью lvalue)
* static_cast!

---

Опять воспользуемся type_traits

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return static_cast<std::remove_reference_t<T>&&>(x);
}
```

Теперь move принимает оба вида value и возвращает всегда rvalue.

Итого теперь мы:

1. Научились делать push_back
2. Научились делать swap
3. Научились передавать временные объекты без копирования

---

## Когда не надо писать std::move

Допустим у нас есть функция get() которая возвращает временный объект (то есть это rvalue)
Тогда не нужно писать f(std::move(get())), можно просто написать f(get())

---

## 13.10. Perfect-forwarding problem и функция std::forward

Надо починить emplace_back. Сейчас он выглядит так:

```c++
template <typename... Args>
void emplace_back(const Args&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

***Note*** Напоминание проблемы: копируем аргументы

Первый шаг очевиден: поменяем тип на универсальную ссылку

---

```c++
template <typename... Args>
void emplace_back(Args&&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Теперь тип каждого аргумента в "пачке" будет либо lvalue ref либо rvalue ref.
При этом тип выражения args это lvalue (идентефикатор).

Написать std::move(args...) нельзя, так как все аргументы мувнуться, а нам нужно мувать только то что было rvalue

---

Для решения этой проблемы используется функция std::forward:

```c++
template <typename... Arguments>
void emplace_back(Arguments&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::forward<Arguments>(args)...);
  size_ += 1;
}
```

***Note*** Заметьте, что шаблонный параметр надо указывать явно

Попробуем ее написать :)

---

Заготовка:

```c++
template <typename T>
? forward(? value) {
    return ?;
}
```

Нужно чтобы:

1. Принималось lvalue
2. Отдавалось rvalue или lvalue в зависимости от того что было передано изначально

---

Первая идея:

```c++
template <typename T>
T&& forward(T&& value) {
    return value;
}
```

Разберем случаи (arg и Argument из emplace_back):

1. arg = lvalue:
   - Argument = type&
   - decltype(arg) = type&
   - T = type&
   - T&& = type&
   - decltype(value) = type&
   - return = type&

С lvalue все работает, давайте посмотрим на rvalue

---

```c++
template <typename T>
T&& forward(T&& value) {
    return value;
}
```

2. arg = rvalue:
   - Argument = type
   - decltype(arg) = type&&
   - T = type
   - T&& = type&&
   - decltype(value) = type&& // CE

И получилось CE так как мы принимаем rvalue а передаем lvalue

---

Попробуем принимать lvalue-ref

```c++
template <typename T>
T&& forward(T& value) {
    return value;
}
```

* Теперь пытаемся проинициализировать rvalue с помощью lvalue-ref. Опять провал

---

Добавим щепотку static_cast

```c++
template <typename T>
T&& forward(T& value) {
    return static_cast<T&&>(value);
}
```

И получилась пародия на move.... Но все таки разберем случаи

1. arg = lvalue:
   - Argument = type&
   - decltype(arg) = type&
   - T = type&
   - T& = type&
   - decltype(value) = type&
   - return = type& (так как T& &&)

Для lvalue работает

---

```c++
template <typename T>
T&& forward(T& value) {
    return static_cast<T&&>(value);
}
```

2. arg = rvalue:
   - Argument = type
   - decltype(arg) = type&&
   - T = type
   - T& = type&
   - decltype(value) = type&
   - return = T&& = type&&

И как будто бы все работает, но не совсем)

---

На самом деле реализация forward выглядит так:

```c++
template <typename T>
T&& forward(std::remove_reference_t<T>& x) noexcept {
  return static_cast<T&&>(x);
}
```

Почему так:

1. Вставление type_traits в тип по сути запрещает компилятору автоматически выводить тип (а мы хотим чтобы в forward тип указывали явно)
2. У forward на самом деле есть вторая перегрузка, про это на следующем слайде

---

Представим такую ситуацию:

```c++
template <typename... Arguments>
void emplace_back(Arguments&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::forward<Arguments>(foo<Args>(args))...);
  size_ += 1;
}
```

Предположим что форвардим не просто аргументы, а какую-то функцию от этих аргументов. Тогда в теории может возникнуть ситуация когда мы пытаемся форвардить уже честное rvalue (а forward у нас не умеет принимать rvalue так как принимает lvalue-ссылку)

---

Поэтому появляется еще одна перегрузка:

```c++
template <typename T>
T&& forward(std::remove_reference_t<T>&& x) noexcept {
  static_assert(!std::is_lvalue_reference_v<T>);
  return static_cast<T&&>(x);
}
```

И теперь все работает как надо

---

## 13.11. reference qualifiers

```c++
struct S {
  void f() & {
    std::cout << 1;
  }

  void f() && {
    std::cout << 2;
  }
}

int main() {
  S s;
  s.f() // 1
  std::move(s).f(); // 2
}
```

Работает как const-qualifier

***Note*** если вообще не ставить qualifier то метод будет одинаково работать для обоих видов value.

---

Пример применения:

Есть у нас класс BigInteger. Есть для него оператор +. Мы хотим чтобы выражения вида a + b = 5 не работали.
До C++ 11 это решалось тем, что operator+ возвращал const BigInteger. Начиная с c++11 нужно просто оператор присваивания пометить lvalue qualifier:

```c++
struct BigInteger {
  BigInteger& opeartor=(const BigInteger&) &;
}
```

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)
