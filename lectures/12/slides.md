---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 12.

---

## 11. Стандартные контейнеры

4. Устройство list
5. Устройство map
6. Инвалидация итераторов

---

# 11.4 Устройство list

std::list - двунаправленный список.

Как его реализовывать?

---

## Базовая реализация

```c++
struct Node {
    T value;
    Node* next;
    Node* prev;
}
```

![list_simple](https://gitlab.com/yaishenka/cpp_course/-/raw/main/lectures/12/list_simple.png)

* Как будем реализоывать итератор? А точней end (и begin у пустого списка)

---

## FakeNode


![list_fake_node](https://gitlab.com/yaishenka/cpp_course/-/raw/main/lectures/12/list_fake_node.png)

* Что хранить в полях list'а?
* Какой value хранить в fake_node?
* Что делать когда список пустой?

---

Очевидный путь: храним Node* fake_node:

Проблема: эта нода всегда должна существовать (из-за итераторов). То есть выделяем память на куче в пустом списке.

---

Будем хранить fake_node на стеке, но тогда нужно придумать что класть в value.

1. У T может не быть конструктора по умолчанию
2. Нельзя создавать объект когда нас не просили этого делать

---

Решение: наследование

```c++
struct BaseNode {
    BaseNode* next_node;
    BaseNode* prev_node;
}

struct Node: BaseNode {
    T value;
}
```

---

Тогда в полях можно хранить следующее:

```c++
struct List {
    List() {
        fake_node.next_node = ?;
        fake_node.prev_node = ?;
    }

    BaseNode fake_node;
}
```

Что сохранить в указатели в случае пустого списка?

---

Нужно закольцевать fake_node на саму себя

```c++
struct List {
    List() {
        fake_node.next_node = &fake_node;
        fake_node.prev_node = &fake_node;
    }

    BaseNode fake_node;
}
```

Неочевидный вариант, но красивый:

```c++
struct List {
    List(): fake_node{this, this} {}

    BaseNode fake_node;
}
```

---

## Специфичные методы list

* sort - у list свой сорт, потому что std::sort требует RA итератор. Может быть реализован через merge sort
* merge - сливает два отсортированных списка
* splice - можно вырезать кусок из одного листа и вставить его в другой

---

## 11.5 Устройство std::map

Ассоциативный массив key,value. Методы мы уже обсуждали, поймем как реализовывать

---

Обычно внутри std::map используется красно-черное дерево (см курс алгоритмов). Соответственно выглядит это как-то так:

```c++
template <typename Key, typename Value>
class map {
    struct Node {
        std::pair<Key, Value> kv;
        Nove* left;
        Node* right;
        Node* parent;
        bool is_red;
    }
}
```

Возникает все та же проблема: как делать итераторы в пустой мапке?

---

Воспользуемся тем же хаком что и с листом:

```c++
template <typename Key, typename Value>
class map {
    struct BaseNode {
        Nove* left;
        Node* right;
        Node* parent;
    }
    struct Node: BaseNode {
        std::pair<Key, Value> kv;
        bool is_red;
    }
}
```

Как учитывать fake_node в дереве?

---

Будем считать что fake_node "правее" всех остальных:

![map_fake_node](https://gitlab.com/yaishenka/cpp_course/-/raw/main/lectures/12/map_fake_node.png)

В алгоритме построения дерева не будем ее учитывать. Соответственно флаг "красноты" хранить тоже не будем

Тогда begin это самый левый ребенок дерева, а end это fake_node

---

Есть нюанс: разименование итератора должно возвращать ссылку на нашу пару, но ключи в дереве менять нельзя (построение слетит). Поэтому:

```c++
template <typename Key, typename Value>
class map {
    struct BaseNode {
        Nove* left;
        Node* right;
        Node* parent;
    }
    struct Node: BaseNode {
        std::pair<const Key, Value> kv; // const
        bool is_red;
    }
}
```

Поэтому итерироваться по мапке лучше через auto:

```c++
for (const pair<Key, Value>& kv : map) { // создается временный объект
    kv = ...;
}
```

---

## Специфичные методы map

* lower_bound - возвращает итератор на первый элемент ключ которого >= key
* upper_bound - <= key

---

## Обход map

За сколько работает обход всего map?

* O(n)
* Но почему? Ведь есть подъемы на логарифм (из крайнего правого ребенка в корень)
* Каждую вершину посетим не более 2 раз

---

## 11.6 Инвалидация итераторов

Табличку можно найти [тут](https://en.cppreference.com/w/cpp/container)

---

Спасибо за внимание!

![img](https://cataas.com/cat/gif)