---
marp: true
theme: gaia
footer: 'Программирование на языке C++. Гагаринов Даниил. ВШПИ МФТИ 2024'
paginate: true

---
<style>
{
    font-size: 25px
}
</style>

# Лекция 6.

---

# 5. Шаблоны

9. Basic type-traits
10. Алиасы
11. Variadic templates
12. Правила вывода шаблонных типов
13. Вычисления на шаблонах

---

<!-- header: "5. Шаблоны"-->

## 5.9 Basic type-traits

Шаблоны позволяют нам создавать некие метафункции (функции от типов).

Посмотрим на несколько их них:

---

### 5.9.1 is_same

Хотим уметь писать какой-то такой код:

```c++
template <typename U, typename V>
void f(U x, V y) {
    if (U == V) {
        ...
    }
}
```

Для этого нам понадобится метафункция is_same. Давайте ее реализуем.

Общий подход к реализации таких метафункций следующий:

1. Завести базовый шаблон
2. Сделать специализацию

---

С базовым шаблоном все просто:

```c++
template <typename U, typename V>
struct is_same {
    static const bool value = false;
};
```

Теперь нам нужна специализация для случая, когда U и V совпадают:

```c++
template <typename U>
struct is_same<U,U> {
    static const bool value = true;
};
```

Пользоваться можно так:

```c++
template <typename U, typename V>
void f(U x, V y) {
    if (is_same<U,V>::value) {
        ...
    }
}
```

---

### 5.9.2 remove_const

Допустим у нас есть следующая функция:

```c++
template <typename T>
void foo(T x) {
    T y;
}
```

Если мы вызовем ее вот так:

```c++
foo<const int>(x);
```

То получим CE, так как константа **y** должна быть проинициализированна.

Нужно научиться снимать константность с типа, для этого напишем метафункцию remove_const:

---

Тут все тоже просто: нужно завести базовый шаблон и специализировать его

```c++
template <typename T>
struct remove_const {
    using type = T;
};

template <typename T>
struct remove_const<const T> {
    using type = T;
};
```

Теперь использование:

```c++
template <typename T>
void foo(T x) {
     typename remove_const <T>::type y;
}
```

***Note*** Все метафункции что мы обсудили реализованы в std и находятся в заголовке type_traits

---

## 5.10 Алиасы

Начиная с C++14 существуют шаблонные псевдонимы, поэтому всеми этими метафункциями стало чуть проще пользоваться:

```c++
using remove_const_t = typename remove_const<T>::type
```

Это немного упрощает код)

В С++17 добавили шаблонные константы, поэтому стало еще лучше:

```c++
template <typename U, typename V>
const bool is_same_v = is_same<U, V>::value;
```

---

## 5.11. Variadic templates (since C++11)

Допустим хотим написать функцию print, которая будет выводить в поток произвольное количество аргументов:

```c++
print(1, 2, "test", 2.0);
```

---

<!-- header: "5.11 Variadic templates"-->

В Си такое можно было бы сделать с помощью механизма va_arg. Посмотрим на примере функции которая складывает числа:

```c++
int add_nums(int count...) {
    int result = 0;
    std::va_list args;
    va_start(args, count);
    for (int i = 0; i < count; ++i)
        result += va_arg(args, int);
    va_end(args);
    return result;
}
```

Проблемы:

* В va_arg нужно явно указывать тип. Именно поэтому нужна строка форматирования в printf
* Плохо читается
* В целом не очень удобно

---

Итак variadic template это шаблон с переменным числом параметров. Начнем с базового синтаксиса:

```c++
template <typename... Args>
void print(Args... args) {

}
```

Многоточие означает что количество аргументов переменное. Эти аргументы так же называют "пакетом".

Какая первая проблема в таком коде?

* Копируем аргументы, надо принимать по константной ссылке

---

Исправим эту проблему

```c++
template <typename... Args>
void print(const Args&... args) {

}
```

"Навешивание" модификаторов происходит как обычно

Теперь нужно придумать как обработать эти аргументы.

---

```c++
template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail) {
    std::cout << head << std::endl;
    print(tail...) // это раскрытие пакета
}
```

Смысл такой: "откусим" первый элемент, а потом уже обработаем все остальные.

Давайте посмотрим как раскрывается print от трех элементов (опустим ссылки):

```c++
print<int, double, string>(1, 1.0, "test") =>

void print(int head, double d, string s) {
    print(d, s); // tail...
}
void print(double head, string s) {
    print(s); // tail...
}
void print(string head) {
    print(); // tail...
}
```

Какая проблема?

----

Нужно добавить перегрузку от 0 аргументов:

```c++
void print() {}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail) {
  std::cout << head << std::endl;
  print(tail...)
}
```

* Сколько функций print при вызове print(1, 2, 3, "abc", 5.0) сгенерирует компилятор?
* Ответ - 5

---

Что еще можно делать с tail:

* Можно написать tail... и распаковать пакет
* Распаковывать можно целые выражения с пакетом: ```print((tail + 1)...)``` прибавит к каждому аргументу 1 при подстановке
* Можно узнать размер пакета: ```sizeof...(tail)```

---

Вариадики помогают писать type-traits:

```c++
template <typename First, typename Second, typename... Types>
struct is_same_many { // is_homogeneous
    const static bool value = std::is_same_v<First, Second> && is_same_many<Second, Types...>::value;
};

template <typename First, typename Second>
struct is_same_many<First, Second> { // is_homogeneous
    const static bool value = std::is_same_v<First, Second>;
};
```
---

<!-- header: "5. Шаблоны"-->

## 5.12. Правила вывода шаблонных типов

Для начала посмотрим на способ как посмотреть какой компилятор вывел тип:

```c++
template <typename T>
void f(T x) = delete;

f(1);
```

В таком случае мы поймаем CE в котором компилятор напишет какой тип он вывел.

---

<!-- header: "5.12. Правила вывода шаблонных типов"-->

Попробуем понять какой тип компилятор выведет для следующих переменных:

```c++
template <typename T>
void f(T x) = delete;

int x = 0;
int& y = x;
const int& z = y;
const int c = 0;

f(x); // 1
f(y); // 2
f(z); // 3
f(c); // 4
```

* f(x) -> T = int
* f(y) -> T = int => при шаблонной постановке компилятор отбрасывает &
* f(z) -> T = int
* f(c) -> T = int

---

А теперь добавим ссылку:

```c++
template <typename T>
void f(T& x) = delete;

int x = 0;
int& y = x;
const int& z = y;
const int c = 0;

f(x); // 1
f(y); // 2
f(z); // 3
f(c); // 4
```

* f(x) -> T = int
* f(y) -> T = int, т.к. T& это int& => T = int
* f(z) -> T = const int, т.к. T& это const int& => T = const int
* f(c) -> T = const int

---

И наконец константная ссылка

```c++
template <typename T>
void f(const T& x) = delete;

int x = 0;
int& y = x;
const int& z = y;
const int c = 0;

f(x); // 1
f(y); // 2
f(z); // 3
f(c); // 4
```

* f(x) -> T = int
* f(y) -> T = int
* f(z) -> T = int
* f(c) -> T = int

---

Важный факт: ссылки на ссылку не существует, поэтому в следующем случае выведется просто int&:

```c++
template <typename T>
void f(T& x) = delete;

int x = 1;
int& y = x;
f<int&>(y); // int& & -> int&
```

---

<!-- header: "5. Шаблоны"-->

## 5.13. Вычисления на шаблонах

Иногда бывает нужно заставить компилятор посчитать что-то на этапе компиляции (представим что constexpr не существует). Такие вычисления называются "метапрограммирование".

Классический пример: числа Фибоначчи

```c++
template <int N>
struct Fib {
    static const int value = Fib<N-1>::value + Fib<N-2>::value;
};

template <>
struct Fib<1> {
    static const int value = 1;
};

template <>
struct Fib<0> {
    static const int value = 0;
};
```

---

Еще один пример: проверка простоты числа

```c++
template <int N, int K>
struct Helper {
    static const bool value = (N % K != 0) && Helper<N, K - 1>::value;
};

template <int N>
struct Helper<N, 1> {
    static const bool value = true;
};

template <int N>
struct IsPrime {
    static const bool value = Helper<N, N-1>::value;
};
```

---

Появляется логичный вопрос: что будет если рекурсия шаблонной подстановки слишком большая?

```c++
IsPrime<10000000>::value;
```

* компилятор упадет с ошибкой "recursive template instantiation exceeded maximum depth"
* чтобы повысить глубину шаблонной рекурсии нужно запустить компиляцию с флагом -ftemplate-depth=N
* глубина рекурсии все равно не может быть слишком большой: оперативки компилятору не хватит

---
<!-- header: ""-->

# 6. Наследование

1. Общая идея
2. Private, public и protected наследование
3. Видимость и доступность полей и методов при наследовании
4. Расположение объектов в памяти при наследовании
5. Порядок вызова конструкторов и деструкторов

---

<!-- header: "6. Наследование"-->

## 6.1 Общая идея

***Def*** Наследование (англ. inheritance) — концепция объектно-ориентированного программирования, согласно которой абстрактный тип данных может наследовать данные и функциональность некоторого существующего типа, способствуя повторному использованию компонентов программного обеспечения.

Допустим мы (зачем-то) пишем классы, которые описывают домашних животных. Мы можем унести все общее у них в отдельный класс AbstractAnimal, функциональность которого будут наследовать остальные

---

Пример:

```c++
struct Base {
  int a = 10;

  void f() {
    std::cout << 1;
  }
};

struct Derived: Base {
  int b = 20;
  void g() {
    std::cout << 2;
  }
};

int main() {
  Derived d;
  d.f();
  d.g();
  std::cout << d.a;
  std::cout << d.b;
}
```

---

## 6.2. Private, public и protected наследование

Модификатор наследования задается следующим синтаксисом:

```c++
struct Base {
};

struct Derived: public Base {
};
```

***Note*** второе отличие классов от структур: в классах модификатор наследования по умолчанию private, в структурах - public.

Модификатор наследования влияет на то, с каким модификатором будут поля и методы родителя в наследнике:

1. Public оставляет все как было
2. Private меняет все на private
3. Protected меняет публичное на protected остальное оставляет как было

---

При этом к private полям родителя наследник не может обращаться даже если наследование публичное. К protected полям класса может обращаться сам класс и его наследники. Подведем итог:

* Public наследование - факт наследования известен всем
* Protected наследование - факт наследования известен наследнику и его наследникам
* Private наследование - факт наследования известен только наследнику

---

## 6.3. Видимость и доступность полей и методов при наследовании

```c++
struct Base {
  void foo() { std::cout << 1; }
};

struct Derived: Base {
  void foo() { std::cout << 2; }
};

int main() {
  Derived d; d.foo();
}
```

* Что выведется?
* Ответ - 2
* А если создать Base?
* Ответ - 1

---

Теперь другая ситуация:

```c++
struct Base { int x = 1; };

struct Derived: Base { int x = 2; };

int main() {
  Derived d; d.x;
}
```

* Какой x выберется?
* Ответ - тот который объявлен в Derived
* А какой размер derived?
* Ответ - 8. Потому что хранится два инта

---

Можно явно обратиться к *x* в родителе:

```c++
struct Base { int x = 1; };

struct Derived: Base { int x = 2; };

int main() {
  Derived d;
  d.Base::x;
}
```
---

С методами аналогично, но есть интересный момент:

```c++
struct Base {
  void foo() {
    std::cout << 1;
  }
};

struct Derived: Base {
  void foo(int) {
    std::cout << 2;
  }
};

int main() {
  Derived d;
  d.foo();
}
```

Будет CE потому что так работает поиск имен: метод foo в Derived затмил метод foo родительский.

---

Можно "ввести" метод родителя в публичную область видимости:

```c++
struct Base {
 protected:
  void foo() {
    std::cout << 1;
  }
};

struct Derived: Base {
  using Base::foo;
  void foo(int) {
    std::cout << 2;
  }
};
```

Теперь в Derived есть foo без параметров. Это сработает только если у самого Derived к foo есть доступ (protected/public). При этом Derived может эту foo вывести в public.

---

## 6.4. Расположение объектов в памяти при наследовании

```c++
class Base {
  int a;
  char b;
};

class Derived: Base {
  double c;
};
```

Тогда объект Derived в памяти будет выглядить следующим образом:

* 4 байта инта
* 1 байт чара
* 3 байта пропущено для выравнивания
* 8 байт дабла

Если бы в объекте Base не было полей, то Derived состоял бы просто из одного double (Empty base optimization).

---

### Выравнивание

***Def*** Машинном словом называется единица данных, которая выбрана естественной для данной архитектуры

Процессор считывает из оперативной памяти данные и кладет их в свою память: регистры. Размер регистра это и есть машинное слово.

Соответственно за раз из памяти читается машинное слово: в случае x86-64 это 8 байт.

Представим что у вас есть массив байт

```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
         [    нужно это машинное слово     ]
[     но приходится читать это    ] [              и это              ]
```

Чтобы таких ситуаций не происходило C++ "выравнивает" данные за счет добавления "мусорных" байтов. Выравнивание происходит по 4 байтам

---

Пример:

Для структуры

```c++
struct S {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};
```

Память выделяется так:

```
[    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ]
[  x ] { потерянные байты } [             y           ] [  z ] { потерянные байты }
```

---

## 6.5. Порядок вызова конструкторов и деструкторов

Очевидно что при создании объекта Derived должен сначала как-то создаться объект типа Base.

Если явно не указано какой конструктор Base вызвать то вызовется конструктор по умолчанию.

```c++
struct Base {
  Base() {std::cout << "B"; }
};

struct Derived: Base {
  Derived() {std::cout << "D"; }
};

int main() {
  Derived b;
}
```

---

Деструкторы вызываются в обратном порядке

```c++
struct A {
  A() {std::cout << "A\n"; }
  ~A() {std::cout << "~A\n"; }
};

struct B {
  B() {std::cout << "B\n"; }
  ~B() {std::cout << "~B\n"; }
};

struct Base {
  Base() {std::cout << "Base\n"; }
  ~Base() {std::cout << "~Base\n"; }
  A a;
};

struct Derived: Base {
  Derived() {std::cout << "Derived\n"; }
  ~Derived() {std::cout << "~Derived\n"; }
  B b;
};

int main() {
  Derived d;
}
```

---

Вывод предыдущего примера:

A
Base
B
Derived
~Derived
~B
~Base
~A

То есть сначала создаются все поля Base, потом сам Base, потом поля Derived, потом сам Derived. Разрушение же происходит в обратном порядке.

---

Можно явно указать какой конструктор родителя мы хотим вызвать. Например:

```c++
struct Base {
  Base(int a) {}
};

struct Derived: Base {
  Derived(int a, double d): Base(a), d_(d) {}

  double d_;
};
```

При этом можно пользоваться списком инициализации

---
Спасибо за внимание!

![img](https://cataas.com/cat/gif)
