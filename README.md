# Курс по C++

## Общая информация

Курс читается в ВШПИ МФТИ.

ВАЖНО: Текущий main - поток 2024-2025

## Навигация

- [Конспекты лекций](/lectures/)
- [Конспекты семинаров](/sems/)

## Контакты автора

- [tg](https://t.me/yaishenka)
- [vk](https://vk.com/ya1shenka)
- [github](https://github.com/yaishenka)
