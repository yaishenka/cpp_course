#include <iostream>

int foo(int x) {
  return x * 2;
}

int foo(double x) {
  return 10;
}

int main() {
  foo(10);
  foo(10.0);

  std::cout << "Hello\n";
}
