#pragma once

inline int foo() {
#ifdef SIZE
  return 2;
#else
  return 123;
#endif
}
