#include <iostream>

namespace Impl {

namespace v1 {
  int foo(int x) {
    return x + 2;
  }
}

namespace v2 {
  int foo(int x) {
    return x * 2;
  }
}

namespace v3 {
  int foo(int x) {
    return x * x;
  }
}
}


int main() {
  std::cout << Impl::v1::foo(10) << '\n';
  std::cout << Impl::v2::foo(10) << '\n';
  std::cout << Impl::v3::foo(10) << '\n';
  std::cout << Impl::foo(10) << '\n';
}
