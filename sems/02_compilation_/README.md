# Compilation flags

1. preprocessing
  -- g++ -E main.cpp > main_preproc.cpp

2. translation
  -- g++ -S main.cpp -o main.s

3. assembling
  -- g++ -c main.cpp -o main.o

4. linking
  -- g++ main.o a.o b.o ... -o prog
