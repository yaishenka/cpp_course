#include <iostream>

namespace some_long_name {
  namespace another_name {
    int foo() {
      return 1;
    }
  }
}

namespace al = some_long_name::another_name;

int main() {
  std::cout << al::foo();

}
