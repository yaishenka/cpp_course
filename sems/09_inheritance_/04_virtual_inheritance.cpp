#include <iostream>

struct Granny {
  Granny(int x = 10) {
    std::cout << x << '\n';
  } 
};

struct Mom : virtual Granny {
  Mom() : Granny(2) {}
};

struct Dad : virtual Granny {
  Dad() : Granny(5) {}
};

struct Son : Mom, Dad {};

struct A { A() { std::cout << "A\n"; } };
struct B { B() { std::cout << "B\n"; } };
struct C { C() { std::cout << "C\n"; } };

struct D : A, virtual B, C {};

int main() {
  Son s;
  D d;
}
