#include <iostream>

template <typename T>
struct Base {
  void foo() {
    std::cout << "Foo\n";
  }
};

template <typename T>
struct Derived : Base<T> {
  void bar() {
    // foo()  -> foo -independent name
    Base<T>::foo();  // foo - dependent name
    this->foo();     // this - dependent name
  }
};

int main() {
  Derived<int> d;
  d.bar();
}
