#include <iostream>

struct S {
  public:
    int x;
  protected:
    int y;
  private:
    int z;
};

struct S1 : private S {
  friend class C;
};

class C {
 public:
  void foo() {
    S s;
    S1 s1;

    s.x;
    s1.x;

    s1.y;
    s1.z;
    // s.y;
  }
};

int main() {
  C c;
  c.foo();
}
