#include <iostream>

struct S {
  public:
    int x;
  protected:
    int y;
  private:
    int z;
};

struct S1 : private S {

};

struct S2 : public S1 {
  void foo(::S s) {
    s.x;
    // s.y; CE
  }
};

int main() {
  S2 s;
  s.foo(S{});
}
