#include <iostream>

// NVI - Non Virtual Interface

struct A {
 public:
  void foo(int x = 100) {
    fooImpl(x);   
  }

 private:
  virtual void fooImpl(int x) {
    std::cout << x << '\n';
  }
};

struct B : A {
 private:
  void fooImpl(int x) override {
    std::cout << x + 1 << '\n';
  }
};

int main() {
  A a;
  B b;
  A& ref = b;
  ref.foo();
}
