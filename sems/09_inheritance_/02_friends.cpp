#include <iostream>

struct A;
struct B;

struct A {
 protected:
  int x;

};

void foo(const B&, const A&);

struct B : public A {
  friend void foo(const B& b, const A& a) {
    b.x;
    // a.x; CE
  }
};

int main() {

}
