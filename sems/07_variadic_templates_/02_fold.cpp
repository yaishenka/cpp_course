#include <iostream>
#include <format>

template <typename... Args>
int sum(const Args&... args) {
  return (... + (args * args));
}

template <typename... Args>
void Print(const Args&... args) {
  (std::cout << ... << std::format("{} ", args));
}

int main() {
  std::cout << sum(1, 2, 3, 4, 5) << '\n';
  Print(1, 2, 3, 4, 5);
  std::cout << sum() << '\n';
}
