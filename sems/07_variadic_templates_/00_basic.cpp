#include <iostream>
#include <vector>

int sum() { return 0; }

template <typename T>
T foo(const T& val) { return val + val; } 

template <typename Head, typename... Args>
int sum(const Head& head, const Args&... args) {
  return head + sum((args * args)...);
  // sum(2, 3, 4, 5);
  // sum(2 * 2, 3 * 3, 4 * 4, 5 * 5);
  // 4 + sum(9 * 9, 16 * 16, 25 * 25);
}

int main() {
  std::cout << sum(1, 2, 3, 4, 5) << '\n';
}
