#include <iostream>

int f(int x) { return x; } 
int f(int x, int y) { return x + y; } 
int f(int x, int y, int z) { return x + y + z; } 
int f(int x, int y, int z, int t) { return x + y + z + t; }

template <typename... Args>
int foo(const Args&... args) {
  return f(f(args...) + f(args)...);
  // f(f(1, 2, 3) + f(args)...);
  // f(f(1, 2, 3) + f(1), f(1, 2, 3) + f(2), f(1, 2, 3) + f(3));
  // f(6 + 1, 6 + 2, 6 + 3); == 24
}

template <typename... Args>
int bar(const Args&... args) {
  return f(f(args, args...)...);
  // f(f(args, 1, 2, 3)...);
  // f(f(1, 1, 2, 3), f(2, 1, 2, 3), f(3, 1, 2, 3))
  // f(7, 8, 9) == 24
}

int main() {
  std::cout << foo(1, 2, 3) << '\n';
  std::cout << bar(1, 2, 3) << '\n';
}
