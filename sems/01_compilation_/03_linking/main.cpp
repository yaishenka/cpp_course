#include <iostream>

// #include "FOO.hpp"
// #include "BAR.hpp"

extern int foo(int);

extern int bar(int);

extern int x;

int main() {
  std::cout << foo(x) << ' ' << bar(x) << '\n';
}
