#include <iostream>
#include <type_traits>
#include <vector>

template <typename T>
struct TypeIdentity {
  using Type = T;
};

template <typename T>
using TypeIdentityT = TypeIdentity<T>::Type;

template <typename T>
void foo(std::type_identity_t<T>) {}


int main() {
  // foo(10);
  foo<int>(10);
  
}
