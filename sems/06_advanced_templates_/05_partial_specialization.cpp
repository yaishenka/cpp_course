#include <iostream>

template <typename T, typename U>
struct S {};

template <typename T>
struct S <int, T> {};

template <typename T, typename... Args>
struct S <T, std::tuple<Args...>> {};

template <typename T, typename U>
struct FooImpl {
  static void call(T, U) {
    std::cout << 1 << '\n';
  }
};

template <typename T, typename U>
struct FooImpl<T, const U> {
  static void call(T, const U) {
    std::cout << 2 << '\n';
  }
};

template <typename T, typename U>
void foo(T& t, U& u) {
  return FooImpl<T, U>::call(t, u);
}

int main() {
  int x;
  const int y = 0;
  foo(x, x);
  foo(x, y);
}
