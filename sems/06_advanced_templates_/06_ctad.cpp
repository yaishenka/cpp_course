#include <iostream>
#include <vector>

template <typename T>
struct S {
  template <typename U>
  S(U) {}
  S() {}
  S(int) {}

};

template <typename T>
S(T) -> S<T>;

S() -> S<int>;

S(int) -> S<void>;

template <typename T>
void Print(T) {
  std::cout << __PRETTY_FUNCTION__ << '\n';
}

int main() {
  std::vector<int> v(10, 2);
  std::vector v1(10, 2);

  S s(3.5);
  Print(s);
  S s1(5);
  Print(s1);
  S s2;
  Print(s2);
}
