#include <iostream>
#include <type_traits>

// #define T int

template <typename T>
struct Rational {
  Rational() = default;
  Rational(const T& num, const T& denum)
    : numenator_(num), denumenator_(denum) {}

  Rational(const T& val) : numenator_(val), denumenator_(1) {}

  friend Rational<T> operator*(const Rational<T>&, const Rational<T>&) {
    std::cout << "Called operator *\n";
    return {};
  }

  Rational<T> operator/(const Rational<T>&) {
    std::cout << "Called operator /\n";
    return {};
  }
 private:
  T numenator_{0};
  T denumenator_{0};
};

template <typename T>
Rational<T> operator-(const Rational<T>&, const Rational<T>&) {
  std::cout << "Called operator-\n";
  return {};
}


template <typename T>
Rational<T> operator+(const Rational<T>&, const Rational<std::type_identity_t<T>>&) {
  std::cout << "Called operator+\n";
  return {};
}

int main() {
  Rational<int> r1, r2;
  r1 - r2;
  // r1 - 3; -> CE

  r1 + r2;
  r1 + 3;
  // 3 + r1; -> CE
  
  r1 * r2;
  r1 * 3;
  3 * r1;

  r1 / r2;
  r1 / 3;
  // 3 / r1; -> CE

}
