#include <iostream>
#include <vector>

template <typename T>
void foo(std::vector<T>) {
  
}

template <typename T>
struct Vector {
  using value_type = T;
};

template <typename T>
struct Example {
  using Type = int;
}

template <typename T>
void bar(std::vector<T>::value_type) {}

template <typename T>
void baz(Example<T>::Type) {}

int main() {
  std::vector<int> v;
  foo(v);
  bar<int>(10);
}
