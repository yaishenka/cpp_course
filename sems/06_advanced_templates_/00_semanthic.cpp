#include <iostream>
#include <vector>

template <typename T>
void bar(T t) {}

template <typename T>
std::vector<T> foo(T t) {
  bar(t);
}

template <typename T>
void foo(T&, std::vector<std::vector<T>>&) {}

void foo(int) {}

int main() {
  foo<double>(3);
}
