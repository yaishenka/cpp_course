#include <iostream>

template <typename T>
void foo(T) {
  std::cout << "T\n";
}

struct S {};

template <typename T>
void call(T t, S s) {
  foo(t);
  foo(s);
}

void foo(S) {
  std::cout << "S\n";
}

int main() {
  S s;
  call(s, s);
}
