#include <iostream>


template <typename T>
void foo(T) {
  std::cout << 1 << '\n';
}

template <typename T>
void foo(T*) {

  std::cout << 2 << '\n';
}

template <>
void foo(int) {
  std::cout << 3 << '\n';
}

template <typename T, typename U>
void bar(T, U) {
  std::cout << 1 << '\n';
}

template <typename T, typename U>
void bar(T*, U*) {
  std::cout << 2 << '\n';
}


template <>
void bar<int*, int*>(int*, int*) {
  std::cout << 3 << '\n';
}

template <typename T, typename U>
void baz(T, U) {
  std::cout << 1 << '\n';
}

template <>
void baz(int*, int*) {
  std::cout << 2 << '\n';
}

template <typename T, typename U>
void baz(T*, U*) {
  std::cout << 3 << '\n';
}

template <>
void baz(int*, int*) {
  std::cout << 4 << '\n';
}

int main() {
  foo(10);
  int* ptr = nullptr;
  bar(ptr, ptr);
  baz(ptr, ptr);
  baz<int*, int*>(ptr, ptr);
}
