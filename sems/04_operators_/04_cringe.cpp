#include <iostream>

struct B{};

struct A {
  int operator<=>(const B&) const { return 0; }
};

int main() {
  A a;
  B b;
  std::cout << std::boolalpha;
  std::cout << (typeid(a <=> b) == typeid(int)) <<'\n';
  std::cout << (typeid(b <=> a) == typeid(int)) << '\n';
}
