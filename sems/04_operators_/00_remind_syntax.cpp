#include <iostream>

struct IntHolder {
  int x{0};

  IntHolder operator+(const IntHolder& other) const {
    std::cout << "Called operator+\n";
    std::cout << x + other.x << '\n';
    return {x + other.x};
  }

  IntHolder& operator+=(const IntHolder& other) {
    x += other.x;
    // *this = *this + other;
    return *this;
  }

  IntHolder& operator++() { return *this; }
  IntHolder operator++(int) { return *this; }

  explicit(false) operator double() const {
    std::cout << "Converted to double\n";
    return 3.3;
  }
  
  int operator&() const {
    return 123;
  }
};

void foo(double d) {
  std::cout << "Foo\n";
}

int main() {
  IntHolder a{0}, b{3};
  a + b;
  std::cout << (double)a << '\n';
  foo(static_cast<double>(a));
  foo(a);

  std::cout << &a << '\n';
  std::cout << std::addressof(a) << '\n';
}
