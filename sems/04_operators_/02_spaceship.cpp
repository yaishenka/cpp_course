#include <compare>
#include <ios>
#include <iostream>

int main() {
  std::cout << std::boolalpha << '\n';
  std::cout << (typeid(std::strong_ordering) == typeid(3 <=> 5)) << '\n';
  std::cout << (typeid(std::partial_ordering) == typeid(3. <=> 5.)) << '\n';

  std::cout << ((3 <=> 5) == std::strong_ordering::less) << '\n';
}
