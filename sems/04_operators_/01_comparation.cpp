#include <iostream>
#include <type_traits>
#include <utility>

struct IntHolder {
  int x{0};

  bool operator==(const IntHolder& other) const {
    return x == other.x;
  }
};


int main() {
  IntHolder a{1}, b{2};
  std::cout << std::boolalpha << '\n';

  using namespace std::rel_ops;
  std::cout << (a == b) << '\n';
  std::cout << (a != b) << '\n';
}
