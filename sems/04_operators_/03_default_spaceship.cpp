#include <compare>
#include <ios>
#include <iostream>


struct Int {
  int x;
  int y;

  std::strong_ordering operator<=>(const Int& other) const = default;
  // bool operator==(const Int& other) const = default;
  friend bool operator==(const Int&, const Int&) = default;
};

bool operator==(const Int&, const Int&) {}


int main() {
  Int x{1, 1},
      y{2, 2},
      z{1, 2};
  std::cout << std::boolalpha;
  std::cout << (x < y) << '\n';
  std::cout << (y < x) << '\n';
  std::cout << (x < z) << '\n';
  std::cout << (x == y) << '\n';
}
