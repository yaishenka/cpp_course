#pragma once

#include <cstddef>
#include <iterator>
#include <type_traits>

template <typename T>
class Vector {
public:
    using iterator = T*;
    using const_iterator = const T*;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    Vector() = default;
    Vector(std::size_t size, const T& value) {
        resize(size, value);
    }

    ~Vector() { delete[] data_; }

    iterator begin() { return iterator(data_); }
    const_iterator begin() const { return const_iterator(data_); }
    const_iterator cbegin() const { return const_iterator(data_); }

    reverse_iterator rbegin() { return std::make_reverse_iterator(end()); }
    const_reverse_iterator rbegin() const { return std::make_reverse_iterator(end()); }
    const_reverse_iterator crbegin() const { return std::make_reverse_iterator(cend()); }

    iterator end() { return iterator(data_ + size_); }
    const_iterator end() const { return const_iterator(data_ + size_); }
    const_iterator cend() const { return const_iterator(data_ + size_); }

    reverse_iterator rend() { return std::make_reverse_iterator(begin()); }
    const_reverse_iterator rend() const { return std::make_reverse_iterator(begin()); }
    const_reverse_iterator crend() const { return std::make_reverse_iterator(cbegin()); }

    void push_back(const T& value) {
        if (size_ == capacity_) {
            expand();
        }
        data_[size_++] = value;
    }

    void pop_back() { --size_; }

    T& operator[](std::size_t size) {
        return data_[size];
    }

    const T& operator[](std::size_t size) const {
        return data_[size];
    }

    std::size_t size() const { return size_; }
    std::size_t capacity() const { return capacity_; }
    bool empty() const { return size() == 0; }
    T* data() { return data_; }
    const T* data() const { return data_; }

    void reserve(std::size_t new_capacity) {
        if (capacity_ >= new_capacity) {
            return;
        }
        T* new_body = new T[new_capacity];
        for (std::size_t i = 0; i < size_; ++i) {
            new_body[i] = data_[i];
        }
        delete[] data_;
        data_ = new_body;
        capacity_ = new_capacity;
    }

    void resize(std::size_t new_size, const T& value = T()) {
        if (size_ == new_size) {
            return;
        }

        if (size_ > new_size) {
            resize_to_smaller(new_size);
            return;
        }
        resize_to_larger(new_size, value);
    }

private:
    void expand() {
        reserve(capacity_ == 0 ? 1 : capacity_ * 2);
    }

    void resize_to_smaller(std::size_t new_size) {
        size_ = new_size;
    }

    void resize_to_larger(std::size_t new_size, const T& value) {
        reserve(new_size);
        for (std::size_t i = size_; i < new_size; ++i) {
            data_[i] = value;
        }
        size_ = new_size;
    }

    T* data_{nullptr};
    std::size_t size_{0};
    std::size_t capacity_{0};
};