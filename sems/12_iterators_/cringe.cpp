#include <iostream>
#include <vector>

std::vector<int> foo() {
    std::vector<int> vec {1, 2, 3};
    return vec;
}

std::vector<std::vector<int>> bar() {
    std::vector<std::vector<int>> vec{{1, 2, 3}};
    return vec;
}

int main() {
    std::vector<int> vec{1, 2, 3};
    for (int elem : vec) {
        std::cout << elem << '\n';
    }

    for (int elem : foo()) {
        std::cout << elem << '\n';
    }

    for (std::vector<int> cont = bar().front(); int elem : cont) {
        std::cout << elem << '\n';
    }
}