#include "vector2.hpp"

#include <iostream>
#include <ranges>

int main() {
    Vector<int> vec(10, 2);
    for (std::size_t i = 0; i < vec.size(); ++i) {
        std::cout << vec[i] << '\n';
    }
    std::cout << "By Iterator\n";
    for (Vector<int>::iterator it = vec.begin(); it != vec.end(); ++it) {
        std::cout << *it << '\n';
    }

    std::cout << "By Range Based For\n";

    for (const int& element : vec) {
        std::cout << element << '\n';
    }

    vec.push_back(1000);

    std::cout << "By reverse iterator\n";
    for (Vector<int>::reverse_iterator it = vec.rbegin(); it != vec.rend(); ++it) {
        std::cout << *it << '\n';
    }
}