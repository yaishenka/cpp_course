#include <iostream>

extern "C" {
int foo(int);
}

int main() {
  std::cout << foo(10) << '\n';
}
