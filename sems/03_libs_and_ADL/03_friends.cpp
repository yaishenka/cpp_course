#include <iostream>

// struct S;

// void bar();

class C {
  private:
    int foo(int x) { return x * x; }

  friend struct S;
  friend void bar();
};


struct S {
  void foo() {
    C c;
    std::cout << c.foo(10) << '\n';
  }
};

void bar() {
  C c;
  std::cout << c.foo(100) << '\n';
}

int main() {
  S s;
  s.foo();
  bar();
}
