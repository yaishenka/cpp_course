#include <iostream>

class C {
  friend int foo(C) {
    return 10;
  }
};


int main() {
  //foo();
  //::foo();
  C c;
  std::cout << foo(c) << '\n';
}
