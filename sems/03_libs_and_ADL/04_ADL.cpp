#include <iostream>

namespace A {
  struct S {};
  
  void foo(S) {
    std::cout << "foo called\n";
  }
}


int main() {
  A::S s;
  foo(s);
}
