#include <iostream>


struct S {
  friend int main(S) {   // inline main -> UB
    std::cout << "Hello World\n";
  }
};

void g() {
  main(S{});
}
