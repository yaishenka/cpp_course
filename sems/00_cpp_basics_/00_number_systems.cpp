#include <iostream>

// NOLINT
int main() {
  unsigned int x = 0b011010100110;  // 0b -> binary prefix
  // 011010100110 -> 011 010 100 110 -> 3 2 4 6
  unsigned int y = 03246;           // 0  -> octal prefix
  std::cout << std::boolalpha; 
  std::cout << "x == " << x << '\n';
  std::cout << "x == y is " << (x == y) << '\n';

  // 011010100110 -> 0110 1010 0110 -> 6 A 6
  unsigned int z = 0x6A6;           // 0x -> hexadecimal prefix
  
  std::cout << "x == z is " << (x == z) << '\n';


  std::printf(" %d \n %x \n %o \n", x, x, x);
}
