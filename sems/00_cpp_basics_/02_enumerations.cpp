#include <iostream>

// NOLINT
enum /*Anonimous*/ {
  ZERO,  // 0
  ONE,   // 1
  TWO,   // 2
};

// NOLINT
enum {
  TEN = 10,
  ELEVEN, // 11
  FOUR = 4,
  FIVE,   // 5
  SIX,    // 6
  NINE = 9,
  ANOTHER_TEN, // 10
  ANOTHER_ELEVEN, // 11
};

// NOLINT
enum Colors {
  RED,
  GREEN,
  BLUE
};

// NOLINT
enum Mask : unsigned long long {
  READ = 0b001,
  WRITE = 0b010,
  MODIFY = 0b010,
};

// NOLINT
int main() {
  std::cout << ZERO << ' ' << ONE << ' ' << TWO << '\n';
  std::cout << TEN << ' ' << SIX << ' ' << ANOTHER_TEN << '\n';
  Colors color = GREEN;

  std::cout << color << '\n';
  std::cout << READ << ' ' << (READ | WRITE) << '\n';
}
