#include <iostream>


template <typename T>
void foo() {
  std::cout << __PRETTY_FUNCTION__ << '\n';
}

int main() {
  foo<int>();
  foo<std::string>();
}
