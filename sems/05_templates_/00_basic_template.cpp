#include <iostream>

template <typename T>
T SumOfTwo(const T& lhs, const T& rhs) { return lhs + rhs; }

template int SumOfTwo(const int& lhs, const int& rhs);    // явная инстанциация
template <> int SumOfTwo(const int& lhs, const int& rhs); // объявление специализации
int SumOfTwo(const int& lhs, const int& rhs);             // объявление перегрузки
                                                          //
template <typename T>
void bar() {
  SumOfTwo<T>(2, 3);
}

template <std::size_t I>
void foo() {
  foo<I + 1>();
}


int main() {
//   SumOfTwo<int>(3, 2);  
//   SumOfTwo<long long>(3, 2);  
//   SumOfTwo<double>(3, 2);
  // bar<int>();
  foo<0>();
}
