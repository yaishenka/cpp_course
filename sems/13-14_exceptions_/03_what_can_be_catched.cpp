#include <iostream>

// В "динамической" памяти создается буффер, помещается объект исключения и начинает разворачиваться стек
// На эту память можно взять ссылку

int main() {
  try {
    throw 1;  // не должно отвечать за control flow
  } catch (int& x) {
    std::cout << x << '\n';
  }
  
  // possible conversion: add/remove const volatile &
  try {
    throw 3ll;
    // throw std::abort;
  } catch (std::string) {
    std::cout << "string\n";
  } catch (double) {
    std::cout << "double\n";
  } catch (long long) {
    std::cout << "long long\n";
  } catch (const long long) {
    std::cout << "const long long\n";
  } catch (...) /* поймает что угодно */ {
    std::cout << "caught something\n";
  }
}