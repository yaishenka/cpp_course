#include <iostream>

void foo(int x, int y) {
  if (y == 0) {
    throw std::string("Wrong Number\n");
  }
  if (x / y == 3) {
    std::cout << "Success\n";
  }
}

int bar() {
  try {
    foo(10, 0);
  } catch (int x) {
    std::cout << x << '\n'; 
  }
  return 0;
}

int main() {
  int x = 0;
  try {
    bar();
  } catch (std::string s) {
    std::cout << s << '\n';
  }

  std::cout << "Reached End\n";
  std::cout << x << '\n';
}