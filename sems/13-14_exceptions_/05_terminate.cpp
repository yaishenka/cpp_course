#include <exception>
#include <iostream>

void foo() {
  std::cerr << "My Terminate called\n";
}


int main() {
  std::set_terminate(foo);

  // std::terminate();
  throw 1;
}