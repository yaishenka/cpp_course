#include <iostream>

struct S {
  ~S() noexcept(false) {
    try {
      throw 1;
    } catch (...) {
      
    }
  }
};

int main() {
  int x;
  try {
    S s;
    throw 1;
  } catch (...) {

  }
  std::cout << "here\n";
}