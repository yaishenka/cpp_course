#include <iostream>

// Бросить можно что угодно

int foo() {
  throw std::abort;
}

int main() {
  try {
    foo();  
  } catch ( decltype(std::abort) f ) {
    std::cout << "Catched\n";
    f();
  }
}