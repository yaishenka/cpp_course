#include <exception>
#include <iostream>
#include <stdexcept>

void bar() {
  throw std::runtime_error{"Ayaya"};
}

void foo() {
  try {
    bar();
  } catch (std::exception& ex) {
    std::cout << "Hello\n";
    throw;
  }
}

int main() {
  try {
    foo();
  } catch (std::runtime_error& err) {
    std::cout << "Catched\n";
  } catch (std::exception& err) {
    std::cout << "Exc\n";
  }

  try {
    throw;  // uncaught
  } catch (...) {
    std::cout << "here\n";
  }
}