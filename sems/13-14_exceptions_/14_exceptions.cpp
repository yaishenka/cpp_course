#include <exception>
#include <iostream>
#include <queue>


std::queue<std::exception_ptr> ex_queue;

int foo() {
  try {
    throw 1;
  } catch (...) {
    ex_queue.push(std::current_exception());
    
    // std::rethrow_exception(std::current_exception()); -> Similar to throw;
  }
  return 0;
}

void bar() {
  try {
    throw std::string("Hello\n");
  } catch (...) {
    ex_queue.push(std::current_exception());
  }
}

// std::exception_ptr / std::current_exception() / std::rethrow_exception();

int main() {
  foo();
  bar();
  std::cout << "Here\n";

  while (!ex_queue.empty()) {
    try {
      std::exception_ptr ex = ex_queue.front();
      ex_queue.pop();
      std::rethrow_exception(ex);
    } catch (std::string) {
      std::cout << "str\n";
    } catch (int) {
      std::cout << "int\n";
    }
  }
}