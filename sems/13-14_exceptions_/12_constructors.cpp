#include <iostream>

struct S {
  S() {
    data_ = new int[100];
    //
    throw 3;

    body_ = new int[100];
  }

  ~S() {
    std::cout << "Called\n";
    delete[] data_;
    delete[] body_; 
  }

  int* data_;
  int* body_;
};

struct C {
  C() try : s() {
    s.data_[0] = 10;
  } catch (int x) {
    std::cout << x << '\n';
    // throw;
  } catch (...) {

  }

  S s;
};

int foo() try {
  throw 1;
} catch (...) {
  std::cout << "here\n";
  return 4;
}

int main() {
  std::cout << foo() << '\n';
}