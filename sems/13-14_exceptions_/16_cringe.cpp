#include <iostream>

struct A {};

struct B : A {};
struct C : A {};

struct D : B, C {};

struct AV {};

struct BV : virtual AV {};
struct CV : virtual AV {};

struct DV : BV, CV {};

int main() {
  D d;
  // A& aref = d;
  try {
    throw D{};
  } catch (A& ) {
    std::cout << "catched\n";
  } catch (...) {
    std::cout << "...\n";
  }

  try {
    throw DV{};
  } catch (AV&) {
    std::cout << "Caught\n";
  }

}