#include <exception>
#include <iostream>

struct S {
  ~S() {
    std::cout << std::uncaught_exceptions() << '\n';
  }
};

struct A {
  ~A() {
    std::cout << std::uncaught_exceptions() << '\n';
  }
};

struct B {
  ~B() noexcept(false) {
    try {
      A a;
      throw 1;
    } catch (...) {

    }
  }
};

struct C {
  ~C() noexcept(false) {
    try {
      B b;
      throw 1;
    } catch (...) {
      
    }
  }
};

struct D {
  ~D() noexcept(false) {
    try {
      C c;
      throw 1;
    } catch (...) {
      
    }
  }
};

int main() {
  try {
    D d;
  } catch (...) {
  }
}