#include "../utils/Verbose.hpp"

int main() {
  try {
    Verbose v;
    throw v;
  } catch (Verbose v) {

  }

  Verbose v1;

  try {
    Verbose v;
    throw v;
  } catch (Verbose& v) {

  }

  try {
    throw Verbose{};
  } catch (Verbose&) {

  }

  throw Verbose{};
}