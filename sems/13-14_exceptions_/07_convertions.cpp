#include <iostream>

struct Base {};

struct Derived : Base {};

int main() {
  // objects: add/remove const volatile &
  // pointers: many
  try {
    throw "Hello\n";  // const char[7]
  } catch (const char*) {
    std::cout << "Catched\n";
  }

  Derived d;
  try {
    throw &d;
  } catch (Base*) {
    std::cout << "D catched\n";
  }

  // objects: cast ref from derived to base
  try {
    throw Derived{};
  } catch (Base& b) {
    std::cout << "D& Catched\n";
  }
}