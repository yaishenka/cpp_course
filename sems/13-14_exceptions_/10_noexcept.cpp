#include <exception>
#include <ios>
#include <iostream>

void foo() {
  throw 1;
}

void bar() noexcept;

void baz() noexcept(sizeof(int) > 4) {

}

void FOO() noexcept(noexcept(bar())) {}

void BAR() {}

int main() {
  std::cout << std::boolalpha;
  std::cout << noexcept(foo()) << '\n';
  std::cout << noexcept(5 * 6) << '\n';
  std::cout << noexcept(3 / 0) << '\n';
  std::cout << noexcept(bar()) << '\n';
}