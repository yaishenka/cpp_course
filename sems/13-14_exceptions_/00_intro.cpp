#include <iostream>

void foo(int x, int y) {
  if (y == 0) {
    throw std::string("Wrong Number\n");
  }
  if ( x / y == 3) {
    std::cout << "Success\n";
  }
}

// при броске:
// Выделяется блок памяти, туда укладывается объект, который вы бросили, начинает разворачиваться стек (stack unwinding)

int main() {
  int x;
  foo(6, 2);
  foo(10, 0);
  foo(9, 3);
}