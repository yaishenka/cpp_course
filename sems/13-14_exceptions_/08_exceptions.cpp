#include <exception>
#include <iostream>
#include <stdexcept>
#include <vector>

int main() {
  try {
    std::vector<int> v1;
    v1.at(10);
  } catch (std::out_of_range& ex) {
    std::cout << "Here\n";
    std::cout << ex.what() << '\n';
  } catch (std::exception& ex) {
    std::cout << ex.what() << '\n';
  }
}