#include <iostream>

int main() {
  try {
    try {
      throw 1;
    } catch (int) {
      throw 3;
    } catch (double) {

    } catch (std::string) {

    }
  } catch (int x) {
    std::cout << "Error " << x << "\n";
  }
}