#include <iostream>

void foo() noexcept {
  try {
    throw 1;
  } catch (int x) {
    std::cout << x << '\n';
  }

  throw 3;  // std::terminate();
}

int main() {
  try {
    foo();
  } catch (...) {
    std::cout << "catched\n";
  }
}