#include <iostream>
#include <chrono>

double operator""_abs(long double value) {
  return (value > 10 ? -value : value);
}

std::string operator""_to_str(const char* string) {
  return std::string(string);
}

std::size_t operator""_cringe(const char* str, std::size_t len) {
  return len;
}

template <char... Cs>
std::size_t operator""_count() {
  return sizeof...(Cs);
}


using namespace std::chrono::literals;

int main() {
  std::cout << 13.0_abs << '\n';
  std::cout << 1234_to_str << '\n';
  std::cout << "ksekgchm"_cringe << '\n';
  std::cout << 123456_count << '\n';

  10s;
}
