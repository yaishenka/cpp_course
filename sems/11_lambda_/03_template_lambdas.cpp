#include <iostream>
#include <vector>

int main() {
  auto f = [](auto value) { return value * 2; };
  auto f1 = []<typename T>( const std::vector<T>& vec ) -> std::size_t { return vec.size(); };

  std::vector<int> v{1, 2, 3, 4};
  std::cout << f1(v) << '\n';
}
