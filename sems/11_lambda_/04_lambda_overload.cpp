#include <iostream>
#include <variant>
#include <vector>

template <typename... Args>
struct Overloaded : Args... {
  using Args::operator()...;
  Overloaded(Args... args) {}
};

template <typename... Args>
Overloaded(Args... args) -> Overloaded<Args...>;

int main() {
  std::variant<int, std::string, std::vector<int>> var = 10;
  
  auto visitor = Overloaded(
      [](auto x) { std::cout << "sizeof int = " << sizeof(x) << '\n'; },
      [](std::string s) { std::cout << s << '\n'; },
      []<typename T>(std::vector<T>) { std::cout << "called for vector\n"; }
  );
  std::visit(visitor, var);

  var = "Hello";
  std::visit(visitor, var);


  var = std::vector<int>{1, 2, 3, 4};
  std::visit(visitor, var);
}
