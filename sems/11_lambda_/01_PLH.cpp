#include <iostream>

// lambda closures with empty capture list has convertion to pointer to function

int main() {
  auto f1 = +[](int x){ return x * 2; };  // positive lambda hack
  auto f2 = +[](int x){ return x * x; };
  f1 = f2;

  auto f3 = []{ std::cout << "Hello\n"; };
  f3();
}
