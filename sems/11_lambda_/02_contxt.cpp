#include <iostream>

int a = 0;


auto foo() {
  int b = 1;
  static int c = 2;
  ++c;
  // local non static context
  return [=](int d){
    return a + b + c + d;
  };
}

int main() {
  auto f = foo();
  std::cout << f(10) << '\n';
  a = 10;
  std::cout << f(10) << '\n';
  auto f2 = foo();
  std::cout << f(10) << '\n';
  std::cout << f2(10) << '\n';
}
